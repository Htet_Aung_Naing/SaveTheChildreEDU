package com.stc.lazyDataModel.schoolUpdate;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.stc.mgr.updating.SchoolHistoryMgr;
import com.stc.model.updating.SchoolHistoryData;
import com.stc.model.updating.SchoolHistorySearchData;
import com.stc.model.updating.SchoolUpdateHistoryPaginateData;

public class LazySchoolUpdateHitoryModel extends LazyDataModel<SchoolHistoryData>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1715764862387292806L;
	
	SchoolHistorySearchData schoolSearch;
	String tbName;
	
	public LazySchoolUpdateHitoryModel(SchoolHistorySearchData search,String tbName)
	{
		this.schoolSearch = search;
		this.tbName = tbName;
	}

	
	public SchoolHistorySearchData getSchoolSearch() {
		return schoolSearch;
	}


	public void setSchoolSearch(SchoolHistorySearchData schoolSearch) {
		this.schoolSearch = schoolSearch;
	}


	@Override
	public List<SchoolHistoryData> load(int first, int pageSize,
			String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		
		schoolSearch.setOffset(first);
		schoolSearch.setLimit(pageSize);
		
		SchoolUpdateHistoryPaginateData res = SchoolHistoryMgr.findSchoolHistoryList(schoolSearch,tbName);
		setRowCount(res.getCount());
		setPageSize(schoolSearch.getLimit());
		
		List<SchoolHistoryData> schoolList = res.getSchoolHistoryList();
		
		return schoolList;
	}

}
