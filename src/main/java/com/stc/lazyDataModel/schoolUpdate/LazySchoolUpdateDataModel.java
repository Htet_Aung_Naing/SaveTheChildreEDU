package com.stc.lazyDataModel.schoolUpdate;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.stc.mgr.updating.SchoolSearchMgr;
import com.stc.model.updating.SchoolSearchData;
import com.stc.model.updating.SchoolUpdatePaginateData;
import com.stc.model.updating.SchoolUpdatingData;

public class LazySchoolUpdateDataModel extends LazyDataModel<SchoolUpdatingData> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1715764862387292806L;
	
	SchoolSearchData schoolSearch;
	List<SchoolUpdatingData> schoolList;
	
	public LazySchoolUpdateDataModel(SchoolSearchData search)
	{
		this.schoolSearch = search;
	}

	
	public SchoolSearchData getSchoolSearch() {
		return schoolSearch;
	}


	public List<SchoolUpdatingData> getSchoolList() {
		return schoolList;
	}


	public void setSchoolList(List<SchoolUpdatingData> schoolList) {
		this.schoolList = schoolList;
	}


	public void setSchoolSearch(SchoolSearchData schoolSearch) {
		this.schoolSearch = schoolSearch;
	}


	@Override
	public List<SchoolUpdatingData> load(int first, int pageSize,
			String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		
		schoolSearch.setOffset(first);
		schoolSearch.setLimit(pageSize);
		
		SchoolUpdatePaginateData res = SchoolSearchMgr.findSchools(schoolSearch);
		setRowCount(res.getCount());
		setPageSize(schoolSearch.getLimit());
		
		this.schoolList = res.getSchoolList();
		
		return schoolList;
	}

}
