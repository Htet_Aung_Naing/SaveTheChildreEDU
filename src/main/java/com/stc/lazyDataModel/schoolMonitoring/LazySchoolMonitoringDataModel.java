package com.stc.lazyDataModel.schoolMonitoring;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.stc.mgr.monitoring.SchoolMonitoringSearchMgr;
import com.stc.model.monitoring.SchoolMonitoringData;
import com.stc.model.monitoring.SchoolMonitoringPaginateData;
import com.stc.model.monitoring.SchoolMonitoringSearchData;

public class LazySchoolMonitoringDataModel extends LazyDataModel<SchoolMonitoringData> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1715764862387292806L;
	
	SchoolMonitoringSearchData schoolSearch;
	List<SchoolMonitoringData> schoolList;
	
	public LazySchoolMonitoringDataModel(SchoolMonitoringSearchData search)
	{
		this.schoolSearch = search;
	}

	
	public SchoolMonitoringSearchData getSchoolSearch() {
		return schoolSearch;
	}


	public List<SchoolMonitoringData> getSchoolList() {
		return schoolList;
	}


	public void setSchoolList(List<SchoolMonitoringData> schoolList) {
		this.schoolList = schoolList;
	}


	public void setSchoolSearch(SchoolMonitoringSearchData schoolSearch) {
		this.schoolSearch = schoolSearch;
	}


	@Override
	public List<SchoolMonitoringData> load(int first, int pageSize,
			String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		
		schoolSearch.setOffset(first);
		schoolSearch.setLimit(pageSize);
		
		SchoolMonitoringPaginateData res = SchoolMonitoringSearchMgr.findSchools(schoolSearch);
		setRowCount(res.getCount());
		setPageSize(schoolSearch.getLimit());
		
		this.schoolList = res.getSchoolList();
		
		return schoolList;
	}

}
