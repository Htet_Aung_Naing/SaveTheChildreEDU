package com.stc.mgr.monitoring;

import java.sql.Connection;

import com.stc.dao.monitoring.SchoolMonitoringSearchDao;
import com.stc.model.monitoring.SchoolMonitoringData;
import com.stc.model.monitoring.SchoolMonitoringPaginateData;
import com.stc.model.monitoring.SchoolMonitoringSearchData;
import com.stc.util.DataBaseConnection;

public class SchoolMonitoringSearchMgr 
{
	
	public static SchoolMonitoringPaginateData findSchools(SchoolMonitoringSearchData search)
	{
		SchoolMonitoringPaginateData reslist = new SchoolMonitoringPaginateData();
		Connection con = DataBaseConnection.getConnection();
		reslist = SchoolMonitoringSearchDao.findSchool(search, con);		
		return reslist;
	}
	
	public static boolean isValid(String  code)
	{
		boolean flag = false;
		SchoolMonitoringData res = new  SchoolMonitoringData();
		res = SchoolMonitoringSearchDao.getSchoolById(code);
		
		if(res == null)
			flag = true;
		else flag = false;
		
		return flag;
	}
	
	
	public static boolean isValidUpdate(String  code , int id)
	{
		boolean flag = false;
		SchoolMonitoringData res = new  SchoolMonitoringData();
		res = SchoolMonitoringSearchDao.isValidSchoolUpdate(code, id);
		
		if(res == null)
			flag = true;
		else flag = false;
		
		return flag;
	}
	
	

}
