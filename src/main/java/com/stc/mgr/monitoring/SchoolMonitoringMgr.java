package com.stc.mgr.monitoring;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import com.stc.dao.monitoring.SchoolMonitoringDao;
import com.stc.dao.monitoring.SchoolMonitoringSearchDao;
import com.stc.dao.monitoring.SchoolSupportDao;
import com.stc.dao.updating.SchoolHistoryDao;
import com.stc.dao.updating.SchoolSurveyDao;
import com.stc.dao.updating.SchoolUpdatingDao;
import com.stc.model.monitoring.SchoolMonitoringData;
import com.stc.model.monitoring.SchoolSupportData;
import com.stc.model.updating.SchoolHistoryData;
import com.stc.util.DataBaseConnection;
import com.stc.util.Result;
import com.stc.util.SurveyData;

public class SchoolMonitoringMgr {
	
	
	public static boolean insert(SchoolMonitoringData school)
	{
		Connection con = DataBaseConnection.getConnection();
		int parentid = 0;
		try {
			Calendar calendar = Calendar.getInstance();
			Result res = new Result();
			java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
			con.setAutoCommit(false);
			school.setTotalMonthlyIncome(school.getOtherIncome()+school.getFeeFromParent()+school.getMonthlyRevolvingFund());
			school.setTotalexpense(school.getCaregiverSalary()+school.getGeneralExpense());
			school.setMonthlyBalance(school.getTotalMonthlyIncome() - school.getTotalexpense());
			res = SchoolMonitoringDao.insert(school, con);
			
			if(res.isRes())
			{
				parentid = res.getResparentid();
				if(parentid != 0)
				{
					for (SurveyData survey : school.getSurveyList())
					{
						survey.setParentid(parentid);
						survey.setModifiedDate(startDate);
						survey.setCreatedUserName(school.getCreatedUsername());
						survey.setModifiedUserName(school.getModifiedUsername());
						SchoolSurveyDao.insert(survey, "SCHOOL_MONITORING_SURVEY",con);
						SchoolHistoryData history = new  SchoolHistoryData();
						history = SchoolUpdatingDao.updateHistory(survey);				
						SchoolHistoryDao.insert(history, "SCHOOL_MONITORING_HISTORY",con);
					}
					
					
					for(SchoolSupportData support : school.getSupportList())
					{
						res = new Result();
						support.setParentid(parentid);
						res = SchoolSupportDao.insert(support, "SCHOOL_SUPPORT", con);
						if(!res.isRes())
							break;
						/*for (SchoolSupporterData setup : support.getSupporterList()) 
						{
							if(res.isRes())
							{	
								setup.setParentid(res.getResparentid());
								SchoolSupporterDao.insert(setup, con);
							}
							else break;
						}*/
					}
				}
				
				if(res.isRes())
				{
					con.commit();
					con.close();
					return true;
				}else 
					{
						con.rollback();
						return false;
					}
				
			}else
			{
				con.rollback();
				return false;
			}
	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				con.rollback();
				return false;
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return false;
			}
		}finally {
			DataBaseConnection.close(con);
		}
	}
	
	
	public static SchoolMonitoringData findBycode(String  code)
	{
		SchoolMonitoringData res = new  SchoolMonitoringData();
		res = SchoolMonitoringSearchDao.getSchoolById(code);
		if(res != null)
		{	
			res.setSurveyList(SchoolSurveyDao.getListbyparentid("SCHOOL_monitoring_SURVEY", res.getId()));
			ArrayList<SchoolSupportData> supportList = new ArrayList<SchoolSupportData>();
			supportList = SchoolSupportDao.getListbyparentid(res.getId());
			res.setSupportList(supportList);
		}
		return res;
	}
	
	public static boolean delete(String surveyTbname,String historyTbname,SchoolMonitoringData school)
	{
		boolean flag = false;
		Connection con = DataBaseConnection.getConnection();
		try {
			con.setAutoCommit(false);
			flag = SchoolMonitoringDao.delete(school.getSchoolCode());
			if(flag)
			{
				flag = SchoolSurveyDao.deleteByParentid(surveyTbname, school.getId(), con);
				if(flag)
					flag = SchoolHistoryDao.deleteByParentid(historyTbname,school.getId() , con);
			}
			if(flag)
			{
				flag = SchoolSupportDao.deleteByParentid("SCHOOL_SUPPORT", school.getId(), con);
				/*if(flag)
				{
					for (SchoolSupportData support : school.getSupportList()) 
					{
						flag = SchoolSupporterDao.deleteByParentid("SCHOOL_MONITORING_SUPPORTER", support.getId(), con);
						if(!flag)
							break;
					}
				}*/
			}
			
			if(flag)
				con.commit();
			else con.rollback();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;
		}finally {
			DataBaseConnection.close(con);
		}
		
		
		return flag;
	}
	
	
	public static boolean update(SchoolMonitoringData school,String tableName, String histblName)
	{
		
		Connection con = DataBaseConnection.getConnection();
		Calendar calendar = Calendar.getInstance();
		
		java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
		try {
			con.setAutoCommit(false);
			school.setTotalMonthlyIncome(school.getOtherIncome()+school.getFeeFromParent()+school.getMonthlyRevolvingFund());
			school.setTotalexpense(school.getCaregiverSalary()+school.getGeneralExpense());
			school.setMonthlyBalance(school.getTotalMonthlyIncome() - school.getTotalexpense());
			
			if(SchoolMonitoringDao.update(school, con))
			{
				if(SchoolSurveyDao.deleteByParentid(tableName,school.getId(), con))
				{
					for (SurveyData survey : school.getSurveyList()) 
					{
						survey.setParentid(school.getId());
						if(survey.getAddnewMaleCount() == null)
							survey.setAddnewMaleCount(0);
						survey.setMalecount(survey.getMalecount()+survey.getAddnewMaleCount());
						if(survey.getAddnewFemaleCount() == null)
							survey.setAddnewFemaleCount(0);
						survey.setFemalecount(survey.getFemalecount()+survey.getAddnewFemaleCount());
						survey.setModifiedUserName(school.getModifiedUsername());
						survey.setModifiedDate(startDate);
						survey.setCreatedDate(school.getCreatedDate());
						survey.setCreatedUserName(school.getCreatedUsername());
						SchoolSurveyDao.insert(survey, tableName, con);
						if(survey.getAddnewFemaleCount()!=0 || survey.getAddnewMaleCount()!=0)
						{
							SchoolHistoryData history = new  SchoolHistoryData();
							history = SchoolUpdatingDao.updateHistory(survey);		
							SchoolHistoryDao.insert(history, histblName, con);
						}
						
					}
				}
				if(SchoolSupportDao.deleteByParentid("school_support", school.getId(), con))
				{
					
					for (SchoolSupportData support : school.getSupportList()) 
					{
						support.setParentid(school.getId());
						Result res = SchoolSupportDao.insert(support, "SCHOOL_SUPPORT", con);
						/*if(res.isRes())
						{
							SchoolSupporterDao.deleteByParentid("SCHOOL_MONITORING_SUPPORTER", support.getId(), con);
							for (SchoolSupporterData supporter : support.getSupporterList()) {
								supporter.setParentid(res.getResparentid());
								SchoolSupporterDao.insert(supporter, con);
							}
						}else break;*/
						
					}
				}
			}
			
			con.commit();
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;
		}finally {
			DataBaseConnection.close(con);
		}
		
		
	}
	
	public static boolean updateFromApi(SchoolMonitoringData school,String tableName, String histblName)
	{
		
		Connection con = DataBaseConnection.getConnection();
		Calendar calendar = Calendar.getInstance();
		Result res = new Result();
		java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
		try
		{
			con.setAutoCommit(false);
			school.setTotalMonthlyIncome(school.getOtherIncome()+school.getFeeFromParent()+school.getMonthlyRevolvingFund());
			school.setTotalexpense(school.getCaregiverSalary()+school.getGeneralExpense());
			school.setMonthlyBalance(school.getTotalMonthlyIncome() - school.getTotalexpense());
			
			if(SchoolMonitoringDao.updateFromApi(school, con))
			{
				SchoolMonitoringData schoolDataFId = SchoolMonitoringSearchDao.getSchoolById(school.getSchoolCode());
				if(schoolDataFId != null)
				{
					school.setId(schoolDataFId.getId());
				}
				ArrayList<SurveyData> surveyList = new ArrayList<SurveyData>();
					surveyList = SchoolSurveyDao.getListbyparentid(tableName , school.getId());
					if(surveyList.size() != 0)
					{
						if(SchoolSurveyDao.deleteByParentid(tableName,school.getId(), con))
						{
							for(int i = 0 ; i < surveyList.size() ; i++ )
							{
								surveyList.get(i).setParentid(school.getId());
				
								if(school.getSurveyList().get(i).getAddnewMaleCount() == null)
									school.getSurveyList().get(i).setAddnewMaleCount(0);
								surveyList.get(i).setMalecount(surveyList.get(i).getMalecount()+school.getSurveyList().get(i).getMalecount());
								if(school.getSurveyList().get(i).getAddnewFemaleCount() == null)
									school.getSurveyList().get(i).setAddnewFemaleCount(0);
								surveyList.get(i).setFemalecount(surveyList.get(i).getFemalecount()+school.getSurveyList().get(i).getFemalecount());
								surveyList.get(i).setModifiedUserName(school.getModifiedUsername());
								surveyList.get(i).setModifiedDate(startDate);
								surveyList.get(i).setCreatedDate(school.getCreatedDate());
								surveyList.get(i).setCreatedUserName(school.getCreatedUsername());
								if(!school.getSurveyList().get(i).getDescription().equals(""))
									surveyList.get(i).setDescription(school.getSurveyList().get(i).getDescription());
								SchoolSurveyDao.insert(surveyList.get(i) , tableName, con);
								if(school.getSurveyList().get(i).getAddnewFemaleCount()!=0 || school.getSurveyList().get(i).getAddnewMaleCount()!=0)
								{
									SchoolHistoryData history = new  SchoolHistoryData();
									history = SchoolUpdatingDao.updateHistory(surveyList.get(i));		
									SchoolHistoryDao.insert(history, histblName, con);
								}
							}
						}
					}

				}
			
				if(SchoolSupportDao.deleteByParentid("school_support", school.getId(), con))
				{
					
					for (SchoolSupportData support : school.getSupportList()) 
					{
						support.setParentid(school.getId());
						res = SchoolSupportDao.insert(support, "SCHOOL_SUPPORT", con);
						/*if(res.isRes())
						{
							SchoolSupporterDao.deleteByParentid("SCHOOL_MONITORING_SUPPORTER", support.getId(), con);
							for (SchoolSupporterData supporter : support.getSupporterList()) {
								supporter.setParentid(res.getResparentid());
								SchoolSupporterDao.insert(supporter, con);
							}
						}else break;*/
						
					}
				}
						
			con.commit();
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;
		}finally {
			DataBaseConnection.close(con);
		}
		
		
	}
	
	
	

}
