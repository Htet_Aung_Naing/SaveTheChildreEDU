package com.stc.mgr.updating;

import java.sql.Connection;

import com.stc.dao.updating.SchoolSearchDao;
import com.stc.dao.updating.SchoolSurveyDao;
import com.stc.model.updating.SchoolSearchData;
import com.stc.model.updating.SchoolUpdatePaginateData;
import com.stc.model.updating.SchoolUpdatingData;
import com.stc.util.DataBaseConnection;

public class SchoolSearchMgr {
	
	public static SchoolUpdatePaginateData findSchools(SchoolSearchData search)
	{
		SchoolUpdatePaginateData reslist = new SchoolUpdatePaginateData();
		Connection con = DataBaseConnection.getConnection();
		reslist = SchoolSearchDao.findSchool(search, con);		
		return reslist;
	}
	
	public static SchoolUpdatingData findBycode(String  code)
	{
		SchoolUpdatingData res = new  SchoolUpdatingData();
		res = SchoolSearchDao.getSchoolById(code);
		if(res != null)
		{	
			res.setSchoolSurveyList(SchoolSurveyDao.getListbyparentid("SCHOOL_UPDATING_SURVEY", res.getId()));
		}
		return res;
	}
	
	public static boolean isValid(String  code)
	{
		boolean flag = false;
		SchoolUpdatingData res = new  SchoolUpdatingData();
		res = SchoolSearchDao.getSchoolById(code);
		
		if(res == null)
			flag = true;
		else flag = false;
		
		return flag;
	}
	
	public static boolean isValidUpdate(String  code , int id)
	{
		boolean flag = false;
		SchoolUpdatingData res = new  SchoolUpdatingData();
		res = SchoolSearchDao.isValidSchoolUpdate(code, id);
		
		if(res == null)
			flag = true;
		else flag = false;
		
		return flag;
	}

}
