package com.stc.mgr.updating;

import java.sql.Connection;

import com.stc.dao.monitoring.SchoolMonitoringDao;
import com.stc.dao.updating.SchoolHistoryDao;
import com.stc.dao.updating.SchoolUpdatingDao;
import com.stc.model.updating.SchoolHistorySearchData;
import com.stc.model.updating.SchoolUpdateHistoryPaginateData;
import com.stc.util.DataBaseConnection;

public class SchoolHistoryMgr {
	
	public static SchoolUpdateHistoryPaginateData findSchoolHistoryList(SchoolHistorySearchData search,String tbName)
	{
		
		SchoolUpdateHistoryPaginateData reslist = new SchoolUpdateHistoryPaginateData();
		Connection con = DataBaseConnection.getConnection();
		int parentid = 0;
		if(tbName.equals("SCHOOL_monitoring_history"))
		{
			parentid = SchoolMonitoringDao.getparentIdbyCode(con, search.getSccode());
			search.setParentid(parentid);
			reslist = SchoolHistoryDao.findSchoolHistoryList(search,tbName, con);	
		}else 
		{
			parentid = SchoolUpdatingDao.getparentIdbyCode(con, search.getSccode());
			search.setParentid(parentid);
			reslist = SchoolHistoryDao.findSchoolHistoryList(search,tbName, con);	
		}
			
		
			
		return reslist;
	}

}
