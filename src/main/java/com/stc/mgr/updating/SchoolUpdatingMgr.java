package com.stc.mgr.updating;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import com.stc.dao.updating.SchoolHistoryDao;
import com.stc.dao.updating.SchoolSearchDao;
import com.stc.dao.updating.SchoolSurveyDao;
import com.stc.dao.updating.SchoolUpdatingDao;
import com.stc.model.updating.SchoolHistoryData;
import com.stc.model.updating.SchoolUpdatingData;
import com.stc.util.DataBaseConnection;
import com.stc.util.Result;
import com.stc.util.SurveyData;

public class SchoolUpdatingMgr {
	
	public static boolean insert(SchoolUpdatingData school)
	{
		Connection con = DataBaseConnection.getConnection();
		int parentid = 0;
		try {
			Calendar calendar = Calendar.getInstance();
			java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
			con.setAutoCommit(false);
			Result res = new Result();
			res  = SchoolUpdatingDao.insert(school, con);
			if(res.isRes())
			{
				parentid = res.getResparentid();
				if(parentid != 0)
				{
					for (SurveyData survey : school.getSchoolSurveyList())
					{
						survey.setParentid(parentid);
						survey.setModifiedDate(startDate);
						survey.setCreatedUserName(school.getCreatedUsername());
						survey.setModifiedUserName(school.getModifiedUsername());
						SchoolSurveyDao.insert(survey, "SCHOOL_UPDATING_SURVEY",con);
						SchoolHistoryData history = new  SchoolHistoryData();
						history = SchoolUpdatingDao.updateHistory(survey);				
						SchoolHistoryDao.insert(history, "SCHOOL_UPDATING_HISTORY",con);
					}
				}
				
				con.commit();
				con.close();
				return true;
			}else
			{
				con.rollback();
				return false;
			}
			
			
				
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				con.rollback();
				return false;
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return false;
			}
		}finally {
			DataBaseConnection.close(con);
		}
	}
	
	public static boolean delete(String surveyTbname,String historyTbname,SchoolUpdatingData school)
	{
		boolean flag = false;
		Connection con = DataBaseConnection.getConnection();
		try {
			con.setAutoCommit(false);
			flag = SchoolUpdatingDao.delete(school.getSchoolCode());
			if(flag)
			{
				flag = SchoolSurveyDao.deleteByParentid(surveyTbname, school.getId(), con);
				if(flag)
					flag = SchoolHistoryDao.deleteByParentid(historyTbname,school.getId() , con);
			}
			
			if(flag)
				con.commit();
			else con.rollback();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;
		}finally {
			DataBaseConnection.close(con);
		}
		
		
		return flag;
	}
	
	public static boolean update(SchoolUpdatingData school,String tableName, String histblName)
	{
		
		Connection con = DataBaseConnection.getConnection();
		Calendar calendar = Calendar.getInstance();
		java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
		try {
			con.setAutoCommit(false);
			
			
			if(SchoolUpdatingDao.update(school, con))
			{
				if(SchoolSurveyDao.deleteByParentid(tableName,school.getId(), con))
				{
					for (SurveyData survey : school.getSchoolSurveyList()) 
					{
						survey.setParentid(school.getId());
						if(survey.getAddnewMaleCount()==null)
							survey.setAddnewMaleCount(0);
						survey.setMalecount(survey.getMalecount()+survey.getAddnewMaleCount());
						if(survey.getAddnewFemaleCount()==null)
							survey.setAddnewFemaleCount(0);
						survey.setFemalecount(survey.getFemalecount()+survey.getAddnewFemaleCount());
						survey.setModifiedUserName(school.getModifiedUsername());
						survey.setModifiedDate(startDate);
						survey.setCreatedDate(school.getCreatedDate());
						survey.setCreatedUserName(school.getCreatedUsername());
						SchoolSurveyDao.insert(survey, tableName, con);
						if(survey.getAddnewFemaleCount()!=0 || survey.getAddnewMaleCount()!=0)
						{
							SchoolHistoryData history = new  SchoolHistoryData();
							history = SchoolUpdatingDao.updateHistory(survey);		
							SchoolHistoryDao.insert(history, histblName, con);
						}
						
					}
				}
			}
			
			con.commit();
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;
		}
		
		
	}
	
	public static boolean updateApi(SchoolUpdatingData school,String tableName, String histblName)
	{
		
		Connection con = DataBaseConnection.getConnection();
		Calendar calendar = Calendar.getInstance();
		java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
		try {
			con.setAutoCommit(false);
			
			if(SchoolUpdatingDao.updateApi(school, con))
			{
				SchoolUpdatingData schoolFId = SchoolSearchDao.getSchoolById(school.getSchoolCode());
				if(schoolFId != null)
				{
					school.setId(schoolFId.getId());
				}
				
				ArrayList<SurveyData> originalSurveyList = SchoolSurveyDao.getListbyparentid(tableName, school.getId());
				
				if(SchoolSurveyDao.deleteByParentid(tableName,school.getId(), con))
				{
					for (SurveyData survey : school.getSchoolSurveyList()) 
					{
						survey.setParentid(school.getId());
						
						for (SurveyData surveyData : originalSurveyList)
						{
							if(surveyData.getTypeId() == survey.getTypeId())
							{
								survey.setAddnewFemaleCount(surveyData.getFemalecount());
								survey.setAddnewMaleCount(surveyData.getMalecount());
								break;
							}
							
						}
						survey.setMalecount(survey.getMalecount()+survey.getAddnewMaleCount());
						survey.setFemalecount(survey.getFemalecount()+survey.getAddnewFemaleCount());
						survey.setModifiedUserName(school.getModifiedUsername());
						survey.setModifiedDate(startDate);
						survey.setCreatedDate(school.getCreatedDate());
						survey.setCreatedUserName(school.getCreatedUsername());
						SchoolSurveyDao.insert(survey, tableName, con);
						if(survey.getMalecount()!=0 || survey.getFemalecount()!=0)
						{
							SchoolHistoryData history = new  SchoolHistoryData();
							history = SchoolUpdatingDao.updateHistory(survey);		
							SchoolHistoryDao.insert(history, histblName, con);
						}
						
					}
				}
			}else{
				return false;
			}
			
			con.commit();
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;
		}
		
		
	}

}
