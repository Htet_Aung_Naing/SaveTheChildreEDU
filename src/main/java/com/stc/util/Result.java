package com.stc.util;

import java.io.Serializable;

public class Result implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4995998923442148153L;
	
	int resparentid;
	boolean res;
	public int getResparentid() {
		return resparentid;
	}
	public void setResparentid(int resparentid) {
		this.resparentid = resparentid;
	}
	public boolean isRes() {
		return res;
	}
	public void setRes(boolean res) {
		this.res = res;
	}
	
	public Result()
	{
		this.res = false;
		this.resparentid = 0;
	}

}
