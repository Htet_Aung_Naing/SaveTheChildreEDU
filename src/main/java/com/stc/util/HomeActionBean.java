package com.stc.util;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;


/**
 * HomeActionBean
 * 
 * @author Than Htay Aung
 */
@ManagedBean ( name = "homeActionBean")
@SessionScoped
public class HomeActionBean  implements Serializable{



	/**
	 * 
	 */
	private static final long serialVersionUID = -9116082691322821597L;

	public void clearMessage() {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(false);
	}
	
	

}
