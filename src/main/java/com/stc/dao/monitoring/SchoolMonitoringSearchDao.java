package com.stc.dao.monitoring;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.stc.dao.updating.SchoolSurveyDao;
import com.stc.model.monitoring.SchoolMonitoringData;
import com.stc.model.monitoring.SchoolMonitoringPaginateData;
import com.stc.model.monitoring.SchoolMonitoringSearchData;
import com.stc.model.monitoring.SchoolSupportData;
import com.stc.util.CommonEnum;
import com.stc.util.DataBaseConnection;
import com.stc.util.SurveyData;

public class SchoolMonitoringSearchDao {
	
	
	public static String getMeetingData(int id)
	{
		String ans = "";
		for(CommonEnum.FrequencyMeeting g : CommonEnum.FrequencyMeeting.values())
		{
			if(id == g.value())
			{
				ans = g.description();
				break;
			}
		}
		
		return ans;
	}
	
	public static String getDistanceBtwServiceSchool(int id)
	{
		String ans = "";
		for(CommonEnum.DistanceEccdBtwSchool g : CommonEnum.DistanceEccdBtwSchool.values())
		{
			if(id == g.value())
			{
				ans = g.description();
				break;
			}
		}
		
		return ans;
	}
	
	public static String getDistanceBtwServiceTown(int id)
	{
		String ans = "";
		for(CommonEnum.DistanceEccdBtwTown g : CommonEnum.DistanceEccdBtwTown.values())
		{
			if(id == g.value())
			{
				ans = g.description();
				break;
			}
		}
		
		return ans;
	}
	
	
	public static SchoolMonitoringPaginateData findSchool(SchoolMonitoringSearchData searchData , Connection con)
	{
		SchoolMonitoringPaginateData res = new SchoolMonitoringPaginateData();
		String filter = getCriteria(searchData);
		List<SchoolMonitoringData> schoolList = new ArrayList<SchoolMonitoringData>();
		String query = "Select * from SCHOOL_MONITORING "+filter;
		
		try {
			
			Statement st;
			st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			 while(rs.next())
			 {
				 SchoolMonitoringData school = new SchoolMonitoringData();
				 ArrayList<SurveyData> surveyList = new ArrayList<SurveyData>();
				 ArrayList<SchoolSupportData>supportList = new ArrayList<SchoolSupportData>();
				 school.setId(rs.getInt("id"));
				 school.setSchoolCode(rs.getString("school_code"));
				 school.setVillagName(rs.getString("village_name"));
				 school.setTowonship(rs.getString("township"));
				 school.setActivityType(rs.getInt("activity_type"));
				 school.setMoniDate(df.format(rs.getDate("monitoring_date")));
				 school.setEstabalDate(df.format(rs.getDate("ESTABLISHED_DATE")));
				 school.setActivityLabel(school.getActivityType()==1?CommonEnum.ActivityStatus.eccd.description():CommonEnum.ActivityStatus.ape.description());
				 school.setBuildingsize(rs.getString("building_size"));
				 school.setCompundSize(rs.getString("compound_size"));
				 school.setEccdServiceTownDistance(rs.getInt("distance_type_service_town"));
				 school.setEccdServiceSchoolDistace(rs.getInt("distance_type_service_school"));
				 school.setFrequencyMeetingType(rs.getInt("meeting_type"));
				 school.setAmountrevolvingfund(rs.getDouble("revolve_fund"));
				 school.setMonthlyRevolvingFund(rs.getDouble("monthly_revolve_fund"));
				 school.setFeeFromParent(rs.getDouble("parent_fee"));
				 school.setOtherIncome(rs.getDouble("other_income"));
				 school.setTotalMonthlyIncome(rs.getDouble("monthly_income"));
				 school.setCaregiverSalary(rs.getDouble("caregiver_salary"));
				 school.setIssue(rs.getString("issue_problem"));
				 school.setCompleteResolve(rs.getInt("complete_resolve"));
				 school.setOpenClose(rs.getInt("open_close"));
				 school.setMinScoreOfEccdApe(rs.getString("ECCD_APE_STANDARD"));
				 school.setNoOfToilet(rs.getInt("latrine_num"));
				 school.setGetWater(rs.getInt("getwater"));
				 school.setGeneralExpense(rs.getDouble("general_expense"));
				 school.setTotalexpense(rs.getDouble("total_expense"));
				 school.setMonthlyBalance(rs.getDouble("monthly_balance"));
				 school.setFrequencyMeetinglbl(getMeetingData(school.getFrequencyMeetingType()));
				 school.setEccdServiceSchoolDistancelbl(getDistanceBtwServiceSchool(school.getEccdServiceSchoolDistace()));
				 school.setEccdServiceTownDistancelbl(getDistanceBtwServiceTown(school.getEccdServiceTownDistance()));
				 
				 surveyList = SchoolSurveyDao.getListbyparentid("SCHOOL_MONITORING_SURVEY", school.getId());
				 school.setSurveyList(surveyList);
				 
				 supportList = SchoolSupportDao.getListbyparentid(school.getId());
				 school.setSupportList(supportList);
				 
				 schoolList.add(school);
				 
			 }
			 res.setCount(getTotalSchoolCount(searchData,con));
			 res.setSchoolList(schoolList);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
		
		
		return res;
		
	}
	
	public static int getTotalSchoolCount(SchoolMonitoringSearchData search,Connection con)
	{
		int count = 0;
		String filter = getCountCriteria(search);
		String query = "Select count(*) as count from SCHOOL_MONITORING "+filter;
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	public static String getCriteria(SchoolMonitoringSearchData criteria)
	{
		String filter = " where record_status="+CommonEnum.SchoolStatus.active.value();
		
		if(!criteria.getSccode().equals(""))	
		{
			filter += " and school_code like '%"+criteria.getSccode()+"%'";
			
		}
		if(!criteria.getTownship().equals(""))
		{
			filter += " and township like '%"+criteria.getTownship()+"%'";
		}
		if(criteria.getActivity() != 0)
		{
			
			filter += " and activity_type ="+criteria.getActivity();
		}
		if(!criteria.getVilageName().equals(""))
		{
			filter += " and village_name like '%"+criteria.getVilageName()+"%'";
		}
		if(criteria.getFromMonitoringDate() != null && criteria.getToMonitoringDate() != null)
		{
			filter += " and MONITORING_DATE between '"+criteria.getFromMonitoringDate()+"' and '"+criteria.getToMonitoringDate()+"'";
		}
		if(criteria.getFromEstabllishDate() != null && criteria.getToEstablishDate() != null)
		{
			filter += " and ESTABLISHED_DATE between '"+criteria.getFromEstabllishDate()+"' and '"+criteria.getToEstablishDate()+"'";
		}
		
		if(criteria.getLimit() != 0)
			filter += " limit "+criteria.getOffset()+","+criteria.getLimit();
		
		return filter;
	}
	
	public static String getCountCriteria(SchoolMonitoringSearchData criteria)
	{
		String filter = " where record_status="+CommonEnum.SchoolStatus.active.value();
		
		if(!criteria.getSccode().equals(""))	
		{
			filter += " and school_code like '%"+criteria.getSccode()+"%'";
			
		}
		if(!criteria.getTownship().equals(""))
		{
			filter += " and township like '%"+criteria.getTownship()+"%'";
		}
		if(criteria.getActivity() != 0)
		{
			
			filter += " and activity_type ="+criteria.getActivity();
		}
		if(!criteria.getVilageName().equals(""))
		{
			filter += " and village_name like '%"+criteria.getVilageName()+"%'";
		}
		if(criteria.getFromMonitoringDate() != null && criteria.getToMonitoringDate() != null)
		{
			filter += " and MONITORING_DATE between '"+criteria.getFromMonitoringDate()+"' and '"+criteria.getToMonitoringDate()+"'";
		}
		if(criteria.getFromEstabllishDate() != null && criteria.getToEstablishDate() != null)
		{
			filter += " and ESTABLISHED_DATE between '"+criteria.getFromEstabllishDate()+"' and '"+criteria.getToEstablishDate()+"'";
		}
		
		
		
		return filter;
	}
		
	
	public static SchoolMonitoringData getSchoolById(String id)
	{
		SchoolMonitoringData school = null;
		Connection con = null;
		String query = "Select * from SCHOOL_MONITORING where school_code = '"+id+"' and record_status="+CommonEnum.SchoolStatus.active.value();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		con = DataBaseConnection.getConnection();
		Statement st;
		try {
			st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 if(rs.next())
			 {
				 school = new SchoolMonitoringData();
				 school.setId(rs.getInt("id"));
				 school.setSchoolCode(rs.getString("school_code"));
				 school.setTowonship(rs.getString("township"));
				 school.setActivityType(rs.getInt("activity_type"));
				 school.setVillagName(rs.getString("village_name"));
				 school.setMoniDate(df.format(rs.getDate("monitoring_date")));
				 school.setEstabalDate(df.format(rs.getDate("established_date")));;
				 school.setCreatedDate(rs.getDate("created_date"));
				 school.setCreatedUsername("created_user_name");
				 school.setMonitoringDate(rs.getDate("monitoring_date"));
				 school.setEstablishDate(rs.getDate("established_date"));
				 school.setBuildingsize(rs.getString("building_size"));
				 school.setCompundSize(rs.getString("compound_size"));
				 school.setEccdServiceTownDistance(rs.getInt("distance_type_service_town"));
				 school.setEccdServiceSchoolDistace(rs.getInt("distance_type_service_school"));
				 school.setFrequencyMeetingType(rs.getInt("meeting_type"));
				 school.setAmountrevolvingfund(rs.getDouble("revolve_fund"));
				 school.setMonthlyRevolvingFund(rs.getDouble("monthly_revolve_fund"));
				 school.setFeeFromParent(rs.getDouble("parent_fee"));
				 school.setOtherIncome(rs.getDouble("other_income"));
				 school.setTotalMonthlyIncome(rs.getDouble("monthly_income"));
				 school.setCaregiverSalary(rs.getDouble("caregiver_salary"));
				 school.setIssue(rs.getString("issue_problem"));
				 school.setCompleteResolve(rs.getInt("complete_resolve"));
				 school.setOpenClose(rs.getInt("open_close"));
				 school.setMinScoreOfEccdApe(rs.getString("ECCD_APE_STANDARD"));
				 school.setNoOfToilet(rs.getInt("latrine_num"));
				 school.setGetWater(rs.getInt("getwater"));
				 school.setGeneralExpense(rs.getDouble("general_expense"));
				 school.setTotalexpense(rs.getDouble("total_expense"));
				 school.setMonthlyBalance(rs.getDouble("monthly_balance"));
				 
			 }

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
		
		
		return school;
	}
	
	public static SchoolMonitoringData isValidSchoolUpdate(String id , int sid)
	{
		SchoolMonitoringData school = null;
		Connection con = null;
		String query = "Select * from SCHOOL_MONITORING where school_code = '"+id+"' and id <> "+sid+" and record_status="+CommonEnum.SchoolStatus.active.value();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		con = DataBaseConnection.getConnection();
		Statement st;
		try {
			st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 if(rs.next())
			 {
				 school = new SchoolMonitoringData();
				 school.setId(rs.getInt("id"));
				 school.setSchoolCode(rs.getString("school_code"));
				 school.setTowonship(rs.getString("township"));
				 school.setActivityType(rs.getInt("activity_type"));
				 school.setVillagName(rs.getString("village_name"));
				 school.setMoniDate(df.format(rs.getDate("monitoring_date")));;
				 school.setCreatedDate(rs.getDate("created_date"));
				 school.setCreatedUsername("created_user_name");
				 
			 }

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
		
		
		return school;
	}

	

}
