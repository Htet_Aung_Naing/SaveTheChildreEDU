package com.stc.dao.monitoring;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

import com.stc.model.monitoring.SchoolMonitoringData;
import com.stc.util.CommonEnum;
import com.stc.util.DataBaseConnection;
import com.stc.util.Result;



public class SchoolMonitoringDao {
	
	
	public static boolean delete(String scCode) 
	{
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			    	        
			     query = "Delete from SCHOOL_MONITORING where school_code=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, scCode);
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("School Delete error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	
	public static boolean update(SchoolMonitoringData school , Connection con) {
		PreparedStatement ps = null;
		String query;
		try {
		
				Calendar calendar = Calendar.getInstance();
			     java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
			    	        
			     query = "Update SCHOOL_MONITORING set school_code =?,village_name=?,township=?,activity_type=?"
			     		+ ",monitoring_date=?,modified_user_name=?,modified_date=?,established_date=?"
			     		+ ",building_size=?,compound_size=?,distance_type_service_town=?"
			     		+ ",distance_type_service_school=?,meeting_type=?,revolve_fund=?"
			     		+ ",monthly_revolve_fund=?,parent_fee=?,other_income=?,monthly_income=?"
			     		+ ",caregiver_salary=?,issue_problem=?,complete_resolve=?,open_close=?"
			     		+ ",eccd_ape_standard=?,latrine_num=?,getwater=?,general_expense=?,total_expense=?"
			     		+ ",monthly_balance=? where id=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, school.getSchoolCode());
				ps.setString(i++, school.getVillagName());
				ps.setString(i++, school.getTowonship());
				ps.setInt(i++,school.getActivityType());
				ps.setDate(i++,  new java.sql.Date(school.getMonitoringDate().getTime()));
				ps.setString(i++, school.getModifiedUsername());
				ps.setDate(i++, startDate);
				ps.setDate(i++, new java.sql.Date(school.getEstablishDate().getTime()));
				ps.setString(i++, school.getBuildingsize());
				ps.setString(i++, school.getCompundSize());
				ps.setInt(i++, school.getEccdServiceTownDistance());
				ps.setInt(i++, school.getEccdServiceSchoolDistace());
				ps.setInt(i++, school.getFrequencyMeetingType());
				ps.setDouble(i++, school.getAmountrevolvingfund());
				ps.setDouble(i++, school.getMonthlyRevolvingFund());
				ps.setDouble(i++, school.getFeeFromParent());
				ps.setDouble(i++, school.getOtherIncome());
				ps.setDouble(i++, school.getTotalMonthlyIncome());
				ps.setDouble(i++, school.getCaregiverSalary());
				ps.setString(i++, school.getIssue());
				ps.setInt(i++, school.getCompleteResolve());
				ps.setInt(i++, school.getOpenClose());
				ps.setString(i++, school.getMinScoreOfEccdApe());
				ps.setInt(i++, school.getNoOfToilet());
				ps.setInt(i++, school.getGetWater());
				ps.setDouble(i++, school.getGeneralExpense());
				ps.setDouble(i++, school.getTotalexpense());
				ps.setDouble(i++, school.getMonthlyBalance());
				ps.setInt(i++, school.getId());
				ps.execute();
			      

			    return true;
						
			
		} catch (SQLException ex) {
			System.out.println("SCHOOL_MONITORING Update error -->" + ex.getMessage());
			return false;
		} 
	}
	
	
	public static boolean updateFromApi(SchoolMonitoringData school , Connection con) {
		PreparedStatement ps = null;
		String query;
		try {
		
				Calendar calendar = Calendar.getInstance();
			     java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
			    	        
			     query = "Update SCHOOL_MONITORING set school_code =?,village_name=?,township=?,activity_type=?"
			     		+ ",monitoring_date=?,modified_user_name=?,modified_date=?,established_date=?"
			     		+ ",building_size=?,compound_size=?,distance_type_service_town=?"
			     		+ ",distance_type_service_school=?,meeting_type=?,revolve_fund=?"
			     		+ ",monthly_revolve_fund=?,parent_fee=?,other_income=?,monthly_income=?"
			     		+ ",caregiver_salary=?,issue_problem=?,complete_resolve=?,open_close=?"
			     		+ ",eccd_ape_standard=?,latrine_num=?,getwater=?,general_expense=?,total_expense=?"
			     		+ ",monthly_balance=?,record_status=? where school_code=? and record_status=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, school.getSchoolCode());
				ps.setString(i++, school.getVillagName());
				ps.setString(i++, school.getTowonship());
				ps.setInt(i++,school.getActivityType());
				ps.setDate(i++,  new java.sql.Date(school.getMonitoringDate().getTime()));
				ps.setString(i++, school.getModifiedUsername());
				ps.setDate(i++, startDate);
				ps.setDate(i++, new java.sql.Date(school.getEstablishDate().getTime()));
				ps.setString(i++, school.getBuildingsize());
				ps.setString(i++, school.getCompundSize());
				ps.setInt(i++, school.getEccdServiceTownDistance());
				ps.setInt(i++, school.getEccdServiceSchoolDistace());
				ps.setInt(i++, school.getFrequencyMeetingType());
				ps.setDouble(i++, school.getAmountrevolvingfund());
				ps.setDouble(i++, school.getMonthlyRevolvingFund());
				ps.setDouble(i++, school.getFeeFromParent());
				ps.setDouble(i++, school.getOtherIncome());
				ps.setDouble(i++, school.getTotalMonthlyIncome());
				ps.setDouble(i++, school.getCaregiverSalary());
				ps.setString(i++, school.getIssue());
				ps.setInt(i++, school.getCompleteResolve());
				ps.setInt(i++, school.getOpenClose());
				ps.setString(i++, school.getMinScoreOfEccdApe());
				ps.setInt(i++, school.getNoOfToilet());
				ps.setInt(i++, school.getGetWater());
				ps.setDouble(i++, school.getGeneralExpense());
				ps.setDouble(i++, school.getTotalexpense());
				ps.setDouble(i++, school.getMonthlyBalance());
				ps.setInt(i++, CommonEnum.SchoolStatus.active.value());
				ps.setString(i++, school.getSchoolCode());
				ps.setInt(i++, CommonEnum.SchoolStatus.active.value());
				ps.execute();
			      

			    return true;
						
			
		} catch (SQLException ex) {
			System.out.println("SCHOOL_MONITORING Update error -->" + ex.getMessage());
			return false;
		} 
	}
	
	
	
	public static Result insert(SchoolMonitoringData school, Connection con) {
		PreparedStatement ps = null;
		Result res = new Result();
		String query;
		int parentid = 0;
		try {
			Calendar calendar = Calendar.getInstance();
			 java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
			 
			 query = " insert into SCHOOL_MONITORING (SCHOOL_CODE,VILLAGE_NAME,TOWNSHIP,ACTIVITY_TYPE,RECORD_STATUS,"
			 		+ "MONITORING_DATE,"
			 		+ "CREATED_DATE,MODIFIED_DATE,CREATED_USER_NAME,MODIFIED_USER_NAME,ESTABLISHED_DATE,BUILDING_SIZE,COMPOUND_SIZE,"
			 		+ "DISTANCE_TYPE_SERVICE_TOWN,DISTANCE_TYPE_SERVICE_SCHOOL,MEETING_TYPE,REVOLVE_FUND,MONTHLY_REVOLVE_FUND"
			 		+ ",PARENT_FEE,OTHER_INCOME,MONTHLY_INCOME,CAREGIVER_SALARY,ISSUE_PROBLEM,COMPLETE_RESOLVE"
			 		+ ",OPEN_CLOSE,ECCD_APE_STANDARD,LATRINE_NUM,GETWATER,GENERAL_EXPENSE,TOTAL_EXPENSE,MONTHLY_BALANCE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				        

			int i = 1;
			ps = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			ps.setString(i++, school.getSchoolCode());
			ps.setString(i++, school.getVillagName());
			ps.setString(i++, school.getTowonship());
			ps.setInt(i++, school.getActivityType());
			ps.setInt(i++, school.getRecordStatus());
			ps.setDate(i++, new java.sql.Date(school.getMonitoringDate().getTime()));
			ps.setDate(i++, startDate);
			ps.setDate(i++, startDate);
			ps.setString(i++, school.getCreatedUsername());
			ps.setString(i++, school.getModifiedUsername());
			ps.setDate(i++, new java.sql.Date(school.getEstablishDate().getTime()));
			ps.setString(i++, school.getBuildingsize());
			ps.setString(i++, school.getCompundSize());
			ps.setInt(i++, school.getEccdServiceTownDistance());
			ps.setInt(i++, school.getEccdServiceSchoolDistace());
			ps.setInt(i++, school.getFrequencyMeetingType());
			ps.setDouble(i++, school.getAmountrevolvingfund());
			ps.setDouble(i++, school.getMonthlyRevolvingFund());
			ps.setDouble(i++, school.getFeeFromParent());
			ps.setDouble(i++, school.getOtherIncome());
			ps.setDouble(i++, school.getTotalMonthlyIncome());
			ps.setDouble(i++, school.getCaregiverSalary());
			ps.setString(i++, school.getIssue());
			ps.setInt(i++, school.getCompleteResolve());
			ps.setInt(i++, school.getOpenClose());
			ps.setString(i++, school.getMinScoreOfEccdApe());
			ps.setInt(i++, school.getNoOfToilet());
			ps.setInt(i++, school.getGetWater());
			ps.setDouble(i++, school.getGeneralExpense());
			ps.setDouble(i++, school.getTotalexpense());
			ps.setDouble(i++, school.getMonthlyBalance());
			ps.execute();
			
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next())
				parentid = rs.getInt(1);

			res.setResparentid(parentid);
			res.setRes(true);
			
			return res;
					
		} catch (SQLException ex) {
			System.out.println("School Registration error -->" + ex.getMessage());
			res.setRes(false);
			return res;
		} 
	}
	
	
	public static int getparentIdbyCode(Connection con,String code) {
		PreparedStatement ps = null;
		String query;
		int maxkey = 0;
		try {		    	        
			     query = "Select id as count from SCHOOL_MONITORING where school_code = '"+code+"' and record_status="+CommonEnum.SchoolStatus.active.value();
				ps = con.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				if(rs.next())
					maxkey = rs.getInt("count");

			    return maxkey;		
		} catch (SQLException ex) {
			System.out.println("School monitoring error -->" + ex.getMessage());
			return 0;
		}
	}
		
}
