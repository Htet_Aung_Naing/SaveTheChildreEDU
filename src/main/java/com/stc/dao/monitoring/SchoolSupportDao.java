package com.stc.dao.monitoring;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.stc.model.monitoring.SchoolSupportData;
import com.stc.util.CommonEnum;
import com.stc.util.DataBaseConnection;
import com.stc.util.Result;

public class SchoolSupportDao {
	
	
	public static ArrayList<SchoolSupportData> getListbyparentid(int id )
	{
		ArrayList<SchoolSupportData> resList = new ArrayList<SchoolSupportData>();
		Connection con = null;
		String query = "Select * from SCHOOL_SUPPORT where parentid = "+id+" and record_status="+CommonEnum.SchoolStatus.active.value();
		
		con = DataBaseConnection.getConnection();
		Statement st;
		try {
			st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 while(rs.next())
			 {
				 SchoolSupportData support = new SchoolSupportData();
				 support.setId(rs.getInt("id"));
				 support.setSupport_type(rs.getInt("SUPPORT_TYPE"));
				 support.setSupport_lbl(rs.getString("SUPPORT_VALUE_LBL"));
				 support.setSupporter_name(rs.getString("SUPPORTER_NAME"));
				 //support.setSupporterList(SchoolSupporterDao.getListbyparentid(support.getId()));			 		 
				 resList.add(support);
			 }

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
		return resList;
	}
	
	
		
		public static Result insert(SchoolSupportData support,String tablename,Connection con) {

			PreparedStatement ps = null;
			Result res = new Result();
			String query;
			ResultSet rs ;
			int parentid = 0;
			try {
			
				 query = "insert into "+tablename+" (PARENTID,SCHOOL_CODE,SUPPORT_TYPE"
				 		+ ",SUPPORT_VALUE_LBL,SUPPORTER_NAME,"
				 		+ "RECORD_STATUS)"
				 		+ " VALUES(?,?,?,?,?,?)";
					        

				int i = 1;
		
				ps = con.prepareStatement(query ,  Statement.RETURN_GENERATED_KEYS);
				ps.setInt(i++, support.getParentid());
				ps.setString(i++, support.getSchool_code());
				ps.setInt(i++, support.getSupport_type());
				ps.setString(i++, support.getSupport_lbl());
				ps.setString(i++, support.getSupporter_name());
				ps.setInt(i++, CommonEnum.SchoolMonitoringStatus.active.value());
				
				ps.execute();
				rs = ps.getGeneratedKeys();
				if(rs.next())
					parentid = rs.getInt(1);

				res.setResparentid(parentid);
				res.setRes(true);
				return res;
						
			} catch (SQLException ex) {
				System.out.println("School Support  error -->" + ex.getMessage());
				try {
					con.rollback();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				res.setRes(false);
				return res;
			}
		}

		public static boolean deleteByParentid(String tablename,int id , Connection con)
		{
		
			PreparedStatement ps = null;
			String query;
			query = "delete from "+tablename+" where parentid="+id;
			try {
				ps = con.prepareStatement(query);
				ps.execute();
				return true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				return false;
			}
		
		}
}
