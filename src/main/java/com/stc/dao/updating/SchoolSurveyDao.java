package com.stc.dao.updating;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import com.stc.util.CommonEnum;
import com.stc.util.DataBaseConnection;
import com.stc.util.SurveyData;

public class SchoolSurveyDao {
	
	public static boolean insert(SurveyData survey,String tablename,Connection con) {

		PreparedStatement ps = null;
		String query;
		try {
			
			Calendar calendar = Calendar.getInstance();
			 java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
			 
			 query = "insert into "+tablename+" (TYPE_ID,LABEL,MALE_COUNT,FEMALE_COUNT"
			 		+ ",PARENT_ID,CREATED_DATE,"
			 		+ "RECORD_STATUS,MODIFIED_DATE,CREATED_USER_NAME,MODIFIED_USER_NAME,DESCRIPTION)"
			 		+ " VALUES(?,?,?,?,?,?,?,?,?,?,?)";
				        

			int i = 1;
			ps = con.prepareStatement(query);
			ps.setInt(i++, survey.getTypeId());
			ps.setString(i++, survey.getLabel());
			ps.setInt(i++, survey.getMalecount());
			ps.setInt(i++, survey.getFemalecount());
			ps.setInt(i++, survey.getParentid());
			ps.setDate(i++, startDate);
			ps.setInt(i++, 1);
			ps.setDate(i++, startDate);
			ps.setString(i++, survey.getCreatedUserName());
			ps.setString(i++, survey.getModifiedUserName());
			ps.setString(i++, survey.getDescription());
			ps.execute();

			return true;
					
		} catch (SQLException ex) {
			System.out.println("School Survey error -->" + ex.getMessage());
			try {
				con.rollback();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
	}
	
	public static ArrayList<SurveyData> getListbyparentid(String tablename,int id)
	{
		ArrayList<SurveyData> resList = new ArrayList<SurveyData>();
		Connection con = null;
		String query = "Select * from "+ tablename +" where parent_id = "+id+ " and record_status="+CommonEnum.SchoolStatus.active.value();
		
		con = DataBaseConnection.getConnection();
		Statement st;
		try {
			st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 while(rs.next())
			 {
				 SurveyData survey = new SurveyData();
				 survey.setLabel(rs.getString("label"));
				 survey.setTypeId(rs.getInt("type_id"));
				 survey.setMalecount(rs.getInt("male_count"));
				 survey.setFemalecount(rs.getInt("female_count"));
				 survey.setParentid(rs.getInt("parent_id"));
				 survey.setModifiedDate(rs.getDate("modified_date"));
				 survey.setDescription(rs.getString("description"));
				 resList.add(survey);
			 }

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
		return resList;
	}
	
	public static boolean deleteByParentid(String tablename,int id , Connection con)
	{
		
		PreparedStatement ps = null;
		String query;
		query = "delete from "+tablename+" where parent_id="+id;
		try {
			ps = con.prepareStatement(query);
			ps.execute();
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return false;
		}
		
		
	}
	

}
