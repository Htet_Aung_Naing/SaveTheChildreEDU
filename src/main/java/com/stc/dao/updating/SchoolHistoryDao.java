package com.stc.dao.updating;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import com.stc.model.updating.SchoolHistoryData;
import com.stc.model.updating.SchoolHistorySearchData;
import com.stc.model.updating.SchoolHistoryShowData;
import com.stc.model.updating.SchoolUpdateHistoryPaginateData;
import com.stc.model.updating.SchoolUpdatingData;
import com.stc.util.CommonEnum;
import com.stc.util.DataBaseConnection;

public class SchoolHistoryDao {
	
	public static boolean insert(SchoolHistoryData school , String tablename , Connection con) {
	
		PreparedStatement ps = null;
		String query;
		try {
		
			Calendar calendar = Calendar.getInstance();
			 java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
			 
			 query = "insert into "+tablename+" (TYPE_ID,LABEL,MALE_COUNT,FEMALE_COUNT,"
			 		+ "PARENT_ID,MONITORING_DATE,MODIFIED_USER_NAME,RECORD_STATUS) "
			 		+ "VALUES(?,?,?,?,?,?,?,?)";
				        
			 con.setAutoCommit(false);
			int i = 1;
			ps = con.prepareStatement(query);
			ps.setInt(i++, school.getTypeid());
			ps.setString(i++, school.getLabel());
			ps.setInt(i++, school.getMaleCount());
			ps.setInt(i++, school.getFemaleCount());
			ps.setInt(i++, school.getParentid());
			ps.setDate(i++, startDate);
			ps.setString(i++, school.getModifiedUser());
			ps.setInt(i++, CommonEnum.SchoolStatus.active.value());
			ps.execute(); 
			return true;
					
		} catch (SQLException ex) {
			System.out.println("School History error -->" + ex.getMessage());
			try {
				con.rollback();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;
		}
	}
	
	public static ArrayList<SchoolHistoryData> getListbyparentid(String tablename,int id,Date moni_date)
	{
		ArrayList<SchoolHistoryData> resList = new ArrayList<SchoolHistoryData>();
		Connection con = null;
		String query = "Select * from school_updating_history where parent_id =? and monitoring_date=? and record_status=? ";
		
		con = DataBaseConnection.getConnection();
		PreparedStatement st;
		try {
			int i = 1;
			st = con.prepareStatement(query);
			st.setInt(i++, id);
			st.setDate(i++, moni_date);
			st.setInt(i++, CommonEnum.SchoolStatus.active.value());
			 ResultSet rs = st.executeQuery(query);
			 while(rs.next())
			 {
				SchoolHistoryData history = new SchoolHistoryData();
				history.setId(rs.getInt("id")); 
				history.setTypeid(rs.getInt("type_id"));
				history.setLabel(rs.getString("label"));
				history.setMaleCount(rs.getInt("male_count"));
				history.setFemaleCount(rs.getInt("female_count"));
				history.setModifiedUser("modified_user_name");
				resList.add(history);
			 }

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
		return resList;
	}
	
	public static boolean update(SchoolUpdatingData user) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			
				Calendar calendar = Calendar.getInstance();
			     java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
			    	        
			     query = "Update users_info set user_id =?,user_name=?,modified_date=?,modified_user_name=?"
			     		+ ",description=?,gender=?,role=?,address=?,position=? where user_id=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("User Registration Update error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public static int getMaxid(Connection con) {
		PreparedStatement ps = null;
		String query;
		int maxkey = 0;
		try {		    	        
			     query = "Select max(id) as count from SCHOOL_UPDATING";
			    int i = 1;
				ps = con.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				if(rs.next())
					maxkey = rs.getInt("count");
			      
			    con.close();
			    return maxkey;
			
			
			
		} catch (SQLException ex) {
			System.out.println("User Registration Update error -->" + ex.getMessage());
			return 0;
		}
	}
	
	public static ArrayList<SchoolHistoryShowData> getHistoryListByid(String tablename,String sc_code)
	{
		ArrayList<SchoolHistoryShowData> res = new ArrayList<SchoolHistoryShowData>();
		Connection con = DataBaseConnection.getConnection();
		int parentId = SchoolUpdatingDao.getparentIdbyCode(con, sc_code);
		if(parentId != 0)
		{
			ArrayList<Date> modifiedDateList = new  ArrayList<Date>();
			modifiedDateList = getDistinctDateList(parentId, con);
			for (Date date : modifiedDateList) 
			{
				SchoolHistoryShowData historyShow = new SchoolHistoryShowData();
				historyShow.setModifiedDate(date);
				ArrayList<SchoolHistoryData>historyList = SchoolHistoryDao.getListbyparentid(tablename, parentId, date);
				historyShow.setHistoryLIst(historyList);
			}
		}
		
		return res;
	}
	
	
	public static ArrayList<Date> getDistinctDateList(int parentid, Connection con)
	{
		ArrayList<Date> res = new ArrayList<Date>();
		PreparedStatement ps = null;
		try {		    	        
		    String query = "Select distinct(monitoring_date) as mdate from school_updating_history where parent_id"+parentid;
			ps = con.prepareStatement(query);
			
			ResultSet rs = ps.executeQuery();
			while(rs.next())
			{
				res.add(rs.getDate("mdate"));
			}

		    return res;
		
		
		
		} catch (SQLException ex) {
			System.out.println("User Registration Update error -->" + ex.getMessage());
			return res;
		}
	}
	
	
	public static SchoolUpdateHistoryPaginateData findSchoolHistoryList(SchoolHistorySearchData searchData ,String tbName, Connection con)
	{
		SchoolUpdateHistoryPaginateData res = new SchoolUpdateHistoryPaginateData();
		
		
		List<SchoolHistoryData> schoolList = new ArrayList<SchoolHistoryData>();
		String query = "Select label,male_count,female_count,monitoring_date,modified_user_name from "+tbName+" where "
				+ "parent_id="+searchData.getParentid()+" and record_status="+CommonEnum.SchoolStatus.active.value()+" order by monitoring_date desc"
						+ " limit "+searchData.getOffset()+","+searchData.getLimit();
		
		try {
			
			Statement st;
			st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			 while(rs.next())
			 {
				 SchoolHistoryData schoolHistory = new SchoolHistoryData();
				 schoolHistory.setLabel(rs.getString("label"));
				 schoolHistory.setMaleCount(rs.getInt("male_count"));
				 schoolHistory.setFemaleCount(rs.getInt("female_count"));
				 schoolHistory.setDate((df.format(rs.getDate("monitoring_date"))));
				 schoolHistory.setModifiedUser(rs.getString("modified_user_name"));
				 schoolList.add(schoolHistory);
			 }
			 res.setCount(getTotalSchoolHistoryCount(searchData.getParentid(),tbName,con));
			 res.setSchoolHistoryList(schoolList);
			 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
		
		
		return res;
		
	}
	
	
	public static boolean deleteByParentid(String histablename,int parentid,Connection con) {
		PreparedStatement ps = null;
		String query;
		try {   
			    query = "delete from "+histablename+" where parent_id=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setInt(i++, parentid);
				ps.execute();
				
			    return true;
						
		} catch (SQLException ex) {
			System.out.println("User Delete error -->" + ex.getMessage());
			return false;
		} 
	}
	
	public static int getTotalSchoolHistoryCount(int parentid,String tbname,Connection con)
	{
		int count = 0;
		String query = "Select count(*) as count from "+tbname+" where record_status = 1 and parent_id="+parentid;
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}


}
