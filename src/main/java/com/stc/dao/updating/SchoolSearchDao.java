package com.stc.dao.updating;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.stc.model.updating.SchoolSearchData;
import com.stc.model.updating.SchoolUpdatePaginateData;
import com.stc.model.updating.SchoolUpdatingData;
import com.stc.util.CommonEnum;
import com.stc.util.DataBaseConnection;
import com.stc.util.SurveyData;

public class SchoolSearchDao {
	
	public static SchoolUpdatePaginateData findSchool(SchoolSearchData searchData , Connection con)
	{
		SchoolUpdatePaginateData res = new SchoolUpdatePaginateData();
		String filter = getCriteria(searchData);
		List<SchoolUpdatingData> schoolList = new ArrayList<SchoolUpdatingData>();
		String query = "Select id,school_code,village_name,township,activity_type,monitoring_date from SCHOOL_UPDATING"+filter;
		
		try {
			
			Statement st;
			st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			 while(rs.next())
			 {
				 SchoolUpdatingData school = new SchoolUpdatingData();
				 ArrayList<SurveyData> surveyList = new ArrayList<SurveyData>();
				 school.setId(rs.getInt("id"));
				 school.setSchoolCode(rs.getString("school_code"));
				 school.setVillagName(rs.getString("village_name"));
				 school.setTowonship(rs.getString("township"));
				 school.setActivityType(rs.getInt("activity_type"));
				 school.setMoniDate(df.format(rs.getDate("monitoring_date")));
				 school.setActivityLabel(school.getActivityType()==1?CommonEnum.ActivityStatus.eccd.description():CommonEnum.ActivityStatus.ape.description());
				 surveyList = SchoolSurveyDao.getListbyparentid("SCHOOL_UPDATING_SURVEY", school.getId());
				 school.setSchoolSurveyList(surveyList);
				 schoolList.add(school);
			 }
			 res.setCount(getTotalSchoolCount(searchData,con));
			 res.setSchoolList(schoolList);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
		
		
		return res;
		
	}
	
	
	
	public static int getTotalSchoolCount(SchoolSearchData search,Connection con)
	{
		int count = 0;
		String filter = getCountCriteria(search);
		String query = "Select count(*) as count from SCHOOL_UPDATING "+filter;
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	public static String getCriteria(SchoolSearchData criteria)
	{
		String filter = " where record_status="+CommonEnum.SchoolStatus.active.value();
		
		if(!criteria.getSccode().equals(""))
			
		{
			filter += " and school_code like '%"+criteria.getSccode()+"%'";
			
		}
		if(!criteria.getTownship().equals(""))
		{
			filter += " and township like '%"+criteria.getTownship()+"%'";
		}
		if(criteria.getActivity() != 0)
		{
			
			filter += " and activity_type ="+criteria.getActivity();
		}
		if(!criteria.getVilageName().equals(""))
		{
			filter += " and village_name like '%"+criteria.getVilageName()+"%'";
		}
		
		if(criteria.getLimit() != 0)
			filter += " limit "+criteria.getOffset()+","+criteria.getLimit();
		
		return filter;
	}
	
	public static String getCountCriteria(SchoolSearchData criteria)
	{
		String filter = " where record_status="+CommonEnum.SchoolStatus.active.value();
		
		if(!criteria.getSccode().equals(""))
			
		{
			filter += " and school_code like '%"+criteria.getSccode()+"%'";
			
		}
		if(!criteria.getTownship().equals(""))
		{
			filter += " and township like '%"+criteria.getTownship()+"%'";
		}
		if(criteria.getActivity() != 0)
		{
			
			filter += " and activity_type ="+criteria.getActivity();
		}
		if(!criteria.getVilageName().equals(""))
		{
			filter += " and village_name like '%"+criteria.getVilageName()+"%'";
		}
		
		
		
		return filter;
	}
	
	
	public static SchoolUpdatingData isValidSchoolUpdate(String id , int sid)
	{
		SchoolUpdatingData school = null;
		Connection con = null;
		String query = "Select * from SCHOOL_UPDATING where school_code = '"+id+"' and id <> "+sid+" and record_status="+CommonEnum.SchoolStatus.active.value();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		con = DataBaseConnection.getConnection();
		Statement st;
		try {
			st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 if(rs.next())
			 {
				 school = new SchoolUpdatingData();
				 school.setId(rs.getInt("id"));
				 school.setSchoolCode(rs.getString("school_code"));
				 school.setTowonship(rs.getString("township"));
				 school.setActivityType(rs.getInt("activity_type"));
				 school.setVillagName(rs.getString("village_name"));
				 school.setMoniDate(df.format(rs.getDate("monitoring_date")));;
				 school.setCreatedDate(rs.getDate("created_date"));
				 school.setCreatedUsername("created_user_name");
			 }

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
		
		
		return school;
	}
	
	
	public static SchoolUpdatingData getSchoolById(String id)
	{
		SchoolUpdatingData school = null;
		Connection con = null;
		String query = "Select * from SCHOOL_UPDATING where school_code = '"+id+"' and record_status="+CommonEnum.SchoolStatus.active.value();
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		con = DataBaseConnection.getConnection();
		Statement st;
		try {
			st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 if(rs.next())
			 {
				 school = new SchoolUpdatingData();
				 school.setId(rs.getInt("id"));
				 school.setSchoolCode(rs.getString("school_code"));
				 school.setTowonship(rs.getString("township"));
				 school.setActivityType(rs.getInt("activity_type"));
				 school.setVillagName(rs.getString("village_name"));
				 school.setMoniDate(df.format(rs.getDate("monitoring_date")));;
				 school.setCreatedDate(rs.getDate("created_date"));
				 school.setCreatedUsername("created_user_name");
			 }

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
		
		
		return school;
	}
	

}
