package com.stc.dao.updating;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import com.mysql.jdbc.Statement;
import com.stc.model.updating.SchoolHistoryData;
import com.stc.model.updating.SchoolUpdatingData;
import com.stc.util.CommonEnum;
import com.stc.util.DataBaseConnection;
import com.stc.util.Result;
import com.stc.util.SurveyData;

public class SchoolUpdatingDao {
	
	public static Result insert(SchoolUpdatingData school, Connection con) {
		PreparedStatement ps = null;
		Result res = new Result();
		String query;
		int parentid = 0;
		try {
			Calendar calendar = Calendar.getInstance();
			 java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
			 
			 query = " insert into school_updating (SCHOOL_CODE,VILLAGE_NAME,TOWNSHIP,ACTIVITY_TYPE,RECORD_STATUS,"
			 		+ "MONITORING_DATE,"
			 		+ "CREATED_DATE,MODIFIED_DATE,CREATED_USER_NAME,MODIFIED_USER_NAME) VALUES (?,?,?,?,?,?,?,?,?,?)";
				        

			int i = 1;
			ps = con.prepareStatement(query , Statement.RETURN_GENERATED_KEYS);
			ps.setString(i++, school.getSchoolCode());
			ps.setString(i++, school.getVillagName());
			ps.setString(i++, school.getTowonship());
			ps.setInt(i++, school.getActivityType());
			ps.setInt(i++, school.getRecordStatus());
			ps.setDate(i++, new java.sql.Date(school.getMonitoringDate().getTime()));
			ps.setDate(i++, startDate);
			ps.setDate(i++, startDate);
			ps.setString(i++, school.getCreatedUsername());
			ps.setString(i++, school.getModifiedUsername());			
			ps.execute();
			
			ResultSet rs = ps.getGeneratedKeys();
			if(rs.next())
				parentid = rs.getInt(1);
			res.setResparentid(parentid);
			res.setRes(true);
			
			
			return res;
					
		} catch (SQLException ex) {
			System.out.println("School Registration error -->" + ex.getMessage());
			res.setRes(false);
			return res;
		} 
	}
	
	public static SchoolHistoryData updateHistory(SurveyData survey )
	{
		SchoolHistoryData history = new SchoolHistoryData();
		history.setFemaleCount(survey.getFemalecount());
		history.setMaleCount(survey.getMalecount());
		history.setParentid(survey.getParentid());
		history.setTypeid(survey.getTypeId());
		history.setMonitoringDate(new java.sql.Date(survey.getModifiedDate().getTime()));
		history.setLabel(survey.getLabel());
		history.setModifiedUser(survey.getModifiedUserName());
		return history;
	}
	
	public static boolean update(SchoolUpdatingData school , Connection con) {
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			
				Calendar calendar = Calendar.getInstance();
			     java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
			    	        
			     query = "Update SCHOOL_UPDATING set school_code =?,village_name=?,township=?,activity_type=?"
			     		+ ",monitoring_date=?,modified_user_name=?,modified_date=? where id=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, school.getSchoolCode());
				ps.setString(i++, school.getVillagName());
				ps.setString(i++, school.getTowonship());
				ps.setInt(i++,school.getActivityType());
				ps.setDate(i++,  new java.sql.Date(school.getMonitoringDate().getTime()));
				ps.setString(i++, school.getModifiedUsername());
				ps.setDate(i++, startDate);
				ps.setInt(i++, school.getId());
				ps.execute();
			      
			    con.close();
			    return true;
						
			
		} catch (SQLException ex) {
			System.out.println("User Registration Update error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public static boolean updateApi(SchoolUpdatingData school , Connection con) {
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			
				Calendar calendar = Calendar.getInstance();
			     java.sql.Date startDate = new java.sql.Date(calendar.getTime().getTime());
			    	        
			     query = "Update SCHOOL_UPDATING set school_code =?,village_name=?,township=?,activity_type=?"
			     		+ ",monitoring_date=?,modified_user_name=?,modified_date=? where school_code=? and record_status=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, school.getSchoolCode());
				ps.setString(i++, school.getVillagName());
				ps.setString(i++, school.getTowonship());
				ps.setInt(i++,school.getActivityType());
				ps.setDate(i++,  new java.sql.Date(school.getMonitoringDate().getTime()));
				ps.setString(i++, school.getModifiedUsername());
				ps.setDate(i++, startDate);
				ps.setString(i++, school.getSchoolCode());
				ps.setInt(i++, CommonEnum.SchoolStatus.active.value());
				ps.execute();
			      
			    con.close();
			    return true;
						
			
		} catch (SQLException ex) {
			System.out.println("User Registration Update error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public static int getparentIdbyCode(Connection con,String code) {
		PreparedStatement ps = null;
		String query;
		int maxkey = 0;
		try {		    	        
			     query = "Select id as count from SCHOOL_UPDATING where school_code = '"+code+"' and record_status="+CommonEnum.SchoolStatus.active.value();
				ps = con.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				if(rs.next())
					maxkey = rs.getInt("count");

			    return maxkey;		
		} catch (SQLException ex) {
			System.out.println("User Registration Update error -->" + ex.getMessage());
			return 0;
		}
	}
	
	
	public static boolean delete(String scCode) 
	{
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		try {
			con = DataBaseConnection.getConnection();
			    	        
			     query = "delete from SCHOOL_UPDATING where school_code=?";
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, scCode);
				ps.execute();
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("School Updating Delete error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}


}
