package com.stc.user.mapper;

import com.stc.model.user.LoginUser;



public interface SecurityMapper {
	/**
	 * For Spring Security
	 */
	public LoginUser loadUserByUserId(String username);
}