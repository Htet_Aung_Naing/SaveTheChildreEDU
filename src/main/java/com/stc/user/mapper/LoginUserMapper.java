package com.stc.user.mapper;

import org.apache.ibatis.annotations.Param;

import com.stc.model.user.UserInfo;


/**
 * LoginUserDaoImpl.java
 *
 * @author Ei Ei Swe Minn
 */
public interface LoginUserMapper {

	/**
	 * 
	 * @param id
	 * @return
	 */
	UserInfo selectUser(@Param("userId") String id);

}
