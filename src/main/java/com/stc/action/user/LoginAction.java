package com.stc.action.user;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import com.stc.dao.user.LoginDao;
import com.stc.model.user.UserInfo;
import com.stc.util.SessionUtil;



@ManagedBean(name = "loginAction")
@SessionScoped
public class LoginAction implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5792067548421627556L;
	

	public AuthenticationManager getAuthenticationManager() {
		return authenticationManager;
	}
	public void setAuthenticationManager(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	String userid;
	String pwd;
	int role;
	
	public int getRole() {
		return role;
	}
	public void setRole(int role) {
		this.role = role;
	}

	@ManagedProperty(value = "#{userInfo}")
	UserInfo user;
	
	@ManagedProperty(value = "#{authenticationManager}")
	 AuthenticationManager authenticationManager;
	

	
	
	
	public UserInfo getUser() {
		return user;
	}
	public void setUser(UserInfo user) {
		this.user = user;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	
	public String login()
	{	
		
		try 
		{
		
			Authentication request = new UsernamePasswordAuthenticationToken(userid, pwd);
			
			Authentication result = authenticationManager.authenticate(request);
			SecurityContextHolder.getContext().setAuthentication(result);
			 this.user = LoginDao.validate(userid, pwd);
			
			 user = LoginDao.validate(userid, pwd);
			
			if (user != null) {
				HttpSession session = SessionUtil.getSession();
				session.setAttribute("userid", userid);
				session.setAttribute("role", user.getRole());

				role = user.getRole();
				return "main";
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "User name and password are incorrect!"));
				return null;
			}
		
		} catch (AuthenticationException e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "User name and password are incorrect!"));
			return null;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public String logout() {
		
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();
		SecurityContextHolder.clearContext();
		
		return "login";
	}

}
