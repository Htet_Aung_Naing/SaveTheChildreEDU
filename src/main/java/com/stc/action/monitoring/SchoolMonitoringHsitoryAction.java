package com.stc.action.monitoring;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;

import com.stc.lazyDataModel.schoolUpdate.LazySchoolUpdateHitoryModel;
import com.stc.mgr.monitoring.SchoolMonitoringMgr;
import com.stc.model.monitoring.SchoolMonitoringData;
import com.stc.model.updating.SchoolHistorySearchData;
import com.stc.util.CommonEnum;

@ManagedBean(name = "schoolMonitoringHistoryAction")
@ViewScoped
public class SchoolMonitoringHsitoryAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8040153179287955265L;
	
	
	LazySchoolUpdateHitoryModel historyDatamodel;
	SchoolHistorySearchData historysearch;
	String sc_code ;
	SchoolMonitoringData school;
	
	public SchoolHistorySearchData getHistorysearch() {
		return historysearch;
	}

	public void setHistorysearch(SchoolHistorySearchData historysearch) {
		this.historysearch = historysearch;
	}

	public String getSc_code() {
		return sc_code;
	}

	public void setSc_code(String sc_code) {
		this.sc_code = sc_code;
	}

	public SchoolMonitoringData getSchool() {
		return school;
	}

	public void setSchool(SchoolMonitoringData school) {
		this.school = school;
	}

	public LazySchoolUpdateHitoryModel getHistoryDatamodel() {
		return historyDatamodel;
	}

	public void setHistoryDatamodel(LazySchoolUpdateHitoryModel historyDatamodel) {
		this.historyDatamodel = historyDatamodel;
	}

	
	public void reset(String targetDataTable) {
		resetPagination(targetDataTable);
		loadData();
	}
	
	private void resetPagination(String targetDataTable) {
		DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent("schoolMonitoringHistoryform:schoolHistoryTable");
		d.setFirst(0);
	}
	

	
	@PostConstruct
	public void loadData()
	{
		
			
			school = new SchoolMonitoringData();
			historysearch = new SchoolHistorySearchData();
			
			if(sc_code==null)
			{
				sc_code = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("sc_monitoring_id");
				school = SchoolMonitoringMgr.findBycode(sc_code);
				school.setActivityLabel((school.getActivityType()==1)?CommonEnum.ActivityStatus.eccd.description():CommonEnum.ActivityStatus.ape.description());
				historysearch.setSccode(sc_code);
				historyDatamodel = new LazySchoolUpdateHitoryModel(historysearch,"SCHOOL_monitoring_history");
	
			}
		
	}
	
	
	public String edit(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("sc_monitoring_id", id);
		return "schoolSurveyUpdate";
	}

}
