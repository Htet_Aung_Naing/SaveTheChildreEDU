package com.stc.action.monitoring;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.model.monitoring.SchoolMonitoringData;
import com.stc.util.CommonEnum;

@ManagedBean(name = "schoolMonitoringUpadteCompleteAction")
@ViewScoped
public class SchoolMonitoringUpdateCompleteAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6689140211863682938L;
	
	SchoolMonitoringData school;

	public SchoolMonitoringData getSchool() {
		return school;
	}

	public void setSchool(SchoolMonitoringData school) {
		this.school = school;
	}
	
	public String getDistanceBtwserviceSchoolLbl(int value)
	{
		String ans = "";
		for(CommonEnum.DistanceEccdBtwSchool g : CommonEnum.DistanceEccdBtwSchool.values())
		{
			if(g.value()==value)
			{
				ans = g.description();
				break;
			}
		}
		return ans;
	}
	
	public String getDistanceBtwserviceTownLbl(int value)
	{
		String ans = "";
		for(CommonEnum.DistanceEccdBtwTown g : CommonEnum.DistanceEccdBtwTown.values())
		{
			if(g.value()==value)
			{
				ans = g.description();
				break;
			}
		}
		return ans;
	}
	
	public String getMeetingLbl(int value)
	{
		String ans = "";
		for(CommonEnum.FrequencyMeeting g : CommonEnum.FrequencyMeeting.values())
		{
			if(g.value()==value)
			{
				ans = g.description();
				break;
			}
		}
		return ans;
	}
	
	@PostConstruct
	public void init()
	{
	
			this.school = (SchoolMonitoringData) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("schoolMonitoringData");
			school.setActivityLabel(((school.getActivityType()==1)?CommonEnum.ActivityStatus.eccd.description():CommonEnum.ActivityStatus.ape.description()));
			school.setEccdServiceSchoolDistancelbl(getDistanceBtwserviceSchoolLbl(school.getEccdServiceSchoolDistace()));
			school.setEccdServiceTownDistancelbl(getDistanceBtwserviceTownLbl(school.getEccdServiceTownDistance()));
			school.setFrequencyMeetinglbl(getMeetingLbl(school.getFrequencyMeetingType()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		
	}
	
}
