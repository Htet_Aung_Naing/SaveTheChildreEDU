package com.stc.action.monitoring;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.mgr.monitoring.SchoolMonitoringMgr;
import com.stc.model.monitoring.SchoolMonitoringData;
import com.stc.util.CommonEnum;
import com.stc.util.SurveyData;

@ManagedBean(name = "schoolMonitoringDetailAction")
@ViewScoped
public class SchooMonitoringlDetailAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6689140211863682938L;
	
	SchoolMonitoringData school;
	String sc_code;

	
	public String getSc_code() {
		return sc_code;
	}

	public void setSc_code(String sc_code) {
		this.sc_code = sc_code;
	}

	public SchoolMonitoringData getSchool() {
		return school;
	}

	public void setSchool(SchoolMonitoringData school) {
		this.school = school;
	}
	
	public void setActivityLabel(SchoolMonitoringData school)
	{
		for (SurveyData survey : school.getSurveyList()) 
		{
			for(CommonEnum.SchoolSurveyRequirement g : CommonEnum.SchoolSurveyRequirement.values())
			{
				if(survey.getTypeId() == g.value())
				{
					survey.setLabel(g.description());
					break;
				}
			}
		}
		
	}
	
	public  String getMeetingData(int id)
	{
		String ans = "";
		for(CommonEnum.FrequencyMeeting g : CommonEnum.FrequencyMeeting.values())
		{
			if(id == g.value())
			{
				ans = g.description();
				break;
			}
		}
		
		return ans;
	}
	
	public  String getDistanceBtwServiceSchool(int id)
	{
		String ans = "";
		for(CommonEnum.DistanceEccdBtwSchool g : CommonEnum.DistanceEccdBtwSchool.values())
		{
			if(id == g.value())
			{
				ans = g.description();
				break;
			}
		}
		
		return ans;
	}
	
	public  String getDistanceBtwServiceTown(int id)
	{
		String ans = "";
		for(CommonEnum.DistanceEccdBtwTown g : CommonEnum.DistanceEccdBtwTown.values())
		{
			if(id == g.value())
			{
				ans = g.description();
				break;
			}
		}
		
		return ans;
	}
	
	@PostConstruct
	public void init()
	{
	
			school = new SchoolMonitoringData();
			
			if(sc_code==null)
			{
				sc_code = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("sc_monitoring_id");
				school = SchoolMonitoringMgr.findBycode(sc_code);
				school.setActivityLabel((school.getActivityType()==1)?CommonEnum.ActivityStatus.eccd.description():CommonEnum.ActivityStatus.ape.description());
				school.setFrequencyMeetinglbl(getMeetingData(school.getFrequencyMeetingType()));
				school.setEccdServiceSchoolDistancelbl(getDistanceBtwServiceSchool(school.getEccdServiceSchoolDistace()));
				school.setEccdServiceTownDistancelbl(getDistanceBtwServiceTown(school.getEccdServiceTownDistance()));
				//setActivityLabel(school);
			}
		
		
	}
	
}
