package com.stc.action.monitoring;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.mgr.monitoring.SchoolMonitoringMgr;
import com.stc.mgr.monitoring.SchoolMonitoringSearchMgr;
import com.stc.model.monitoring.SchoolMonitoringData;
import com.stc.model.monitoring.SchoolSupportData;
import com.stc.util.CommonEnum;
import com.stc.util.SessionUtil;
import com.stc.util.SetupData;
import com.stc.util.SurveyData;

@ManagedBean(name = "schoolMonitoringAction")
@ViewScoped
public class SchoolMonitoringAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1921631856969027860L;
	SchoolMonitoringData school;
	List<SetupData> activityList;
	List<SetupData> meetingList;

	List<SetupData> distanceServiceTownList;
	List<SetupData> distanceServiceSchoolList;
	SchoolSupportData schoolSupporttech;
	SchoolSupportData schoolSupportfund;
	SchoolSupportData schoolSupporttlms;
	String score;
	public String getScore() {
		return score;
	}


	public void setScore(String score) {
		this.score = score;
	}

	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	
	
	public SchoolSupportData getSchoolSupporttech() {
		return schoolSupporttech;
	}


	public void setSchoolSupporttech(SchoolSupportData schoolSupporttech) {
		this.schoolSupporttech = schoolSupporttech;
	}


	public SchoolSupportData getSchoolSupportfund() {
		return schoolSupportfund;
	}


	public void setSchoolSupportfund(SchoolSupportData schoolSupportfund) {
		this.schoolSupportfund = schoolSupportfund;
	}


	public SchoolSupportData getSchoolSupporttlms() {
		return schoolSupporttlms;
	}


	public void setSchoolSupporttlms(SchoolSupportData schoolSupporttlms) {
		this.schoolSupporttlms = schoolSupporttlms;
	}


	public void setDistanceServiceSchoolList(
			List<SetupData> distanceServiceSchoolList) {
		this.distanceServiceSchoolList = distanceServiceSchoolList;
	}


	public List<SetupData> getDistanceServiceTownList() {
		return distanceServiceTownList;
	}


	public void setDistanceServiceTownList(List<SetupData> distanceServiceTownList) {
		this.distanceServiceTownList = distanceServiceTownList;
	}


	


	@PostConstruct
	public void init()
	{

			this.school = new SchoolMonitoringData();
			this.school.surveyList = getSurveyData();
			this.activityList = getActivityData();
			this.meetingList = getMeetingData();
			this.school.supportList = getSupportData();
			this.distanceServiceTownList = getDistanceBtwServiceTownList();
			this.distanceServiceSchoolList = getDistanceServiceSchoolList();
			initSupporterData();
		
	}
	
	
	public List<SetupData> getActivityData()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		for(CommonEnum.ActivityStatus g : CommonEnum.ActivityStatus.values())
		{
			SetupData setup = new SetupData();
			setup.setLabel(g.description());
			setup.setValue(g.value());
			res.add(setup);
		}
		
		return res;
	}
	
	public List<SetupData> getMeetingData()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		for(CommonEnum.FrequencyMeeting g : CommonEnum.FrequencyMeeting.values())
		{
			SetupData setup = new SetupData();
			setup.setLabel(g.description());
			setup.setValue(g.value());
			res.add(setup);
		}
		
		return res;
	}
	
	
	
	
	public ArrayList<SurveyData> getSurveyData()
	{
		ArrayList<SurveyData>res = new ArrayList<SurveyData>() ;
		for(CommonEnum.SchoolMonitoringSurveyData g : CommonEnum.SchoolMonitoringSurveyData.values())
		{
			SurveyData suv = new SurveyData();
			suv.setLabel(g.description());
			suv.setTypeId(g.value());
			res.add(suv);
		}
		
		return res;
	}
	
	public void initSupporterData()
	{
		for(CommonEnum.SupportTitle g : CommonEnum.SupportTitle.values())
		{
			if(g.value() == 1)
			{
				schoolSupporttech = new SchoolSupportData();
				schoolSupporttech.setSupport_type(g.value());
				schoolSupporttech.setSupport_lbl(g.description());
			}
			if(g.value() == 2)
			{
				schoolSupportfund = new SchoolSupportData();
				schoolSupportfund.setSupport_type(g.value());
				schoolSupportfund.setSupport_lbl(g.description());
			}
			if(g.value() == 3)
			{
				schoolSupporttlms = new SchoolSupportData();
				schoolSupporttlms.setSupport_type(g.value());	
				schoolSupporttlms.setSupport_lbl(g.description());
			}
		}
	}
	
	public ArrayList<SchoolSupportData> getSupportData()
	{
		ArrayList<SchoolSupportData>res = new ArrayList<SchoolSupportData>() ;
		for(CommonEnum.SupportTitle g : CommonEnum.SupportTitle.values())
		{
			SchoolSupportData support = new SchoolSupportData();
			support.setSupport_type(g.value());
			support.setSupport_lbl(g.description());
			/*ArrayList<SetupData> setupList = new ArrayList<SetupData>();
			for(CommonEnum.SupportValue value : CommonEnum.SupportValue.values())
			{
				SetupData setup = new SetupData();
				setup.setLabel(value.description());
				setup.setValue(value.value());
				setupList.add(setup);
			}
			support.setSupporterList(setupList);*/
			res.add(support);
		}
		
		return res;
	}
	
	
	
	public SchoolMonitoringData getSchool() {
		return school;
	}

	public void setSchool(SchoolMonitoringData school) {
		this.school = school;
	}

	public List<SetupData> getActivityList() {
		return activityList;
	}

	public void setActivityList(List<SetupData> activityList) {
		this.activityList = activityList;
	}

	public List<SetupData> getMeetingList() {
		return meetingList;
	}

	public void setMeetingList(List<SetupData> meetingList) {
		this.meetingList = meetingList;
	}



	public List<SetupData> getDistanceBtwServiceTownList()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		for(CommonEnum.DistanceEccdBtwTown g : CommonEnum.DistanceEccdBtwTown.values())
		{
			SetupData setup = new SetupData();
			setup.setLabel(g.description());
			setup.setValue(g.value());
			res.add(setup);
		}
		
		return res;
	}
	
	public List<SetupData> getDistanceServiceSchoolList()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		for(CommonEnum.DistanceEccdBtwSchool g : CommonEnum.DistanceEccdBtwSchool.values())
		{
			SetupData setup = new SetupData();
			setup.setLabel(g.description());
			setup.setValue(g.value());
			res.add(setup);
		}
		
		return res;
	}
	
	public String save()
	{
		HttpSession session = SessionUtil.getSession();
		String userid = (String) session.getAttribute("userid");
		school.setCreatedUsername(userid);
		school.setModifiedUsername(userid);
		school.setRecordStatus(1);
		school.setSupportList(new ArrayList<SchoolSupportData>());
		school.getSupportList().add(schoolSupporttech);
		school.getSupportList().add(schoolSupportfund);
		school.getSupportList().add(schoolSupporttlms);
		if(SchoolMonitoringSearchMgr.isValid(school.getSchoolCode()))
		{
			SchoolMonitoringMgr.insert(school);
			school.setMoniDate(df.format(school.getMonitoringDate()));
			school.setEstabalDate(df.format(school.getEstablishDate()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("schoolMonitoring", school);
			return "monitoringschoolSurveyInsertSuccess";
		}else 
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "School code must not be same"));
			return null;
		}
		
		
	}
	
	public void handleKeyEvent()
	{
		if(!school.getMinScoreOfEccdApe().equals(""))
		{
			if(Integer.parseInt(school.getMinScoreOfEccdApe()) > 10 && Integer.parseInt(school.getMinScoreOfEccdApe())<30)
			{
				score = "normal";
			}
			else if(Integer.parseInt(school.getMinScoreOfEccdApe()) > 30 && Integer.parseInt(school.getMinScoreOfEccdApe())<50)
			{
				score="high";
			}
			else
			{
				score="";
			}
		}else
		{
			score="";
		}
	}

}
