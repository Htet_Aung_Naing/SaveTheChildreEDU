package com.stc.action.monitoring;


import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.component.datatable.DataTable;

import com.stc.lazyDataModel.schoolMonitoring.LazySchoolMonitoringDataModel;
import com.stc.mgr.monitoring.SchoolMonitoringSearchMgr;
import com.stc.model.monitoring.SchoolMonitoringData;
import com.stc.model.monitoring.SchoolMonitoringPaginateData;
import com.stc.model.monitoring.SchoolMonitoringSearchData;
import com.stc.util.CommonEnum;
import com.stc.util.SessionUtil;
import com.stc.util.SetupData;

@ManagedBean(name = "schoolMonitoringSearchAction")
@ViewScoped
public class SchoolMonitoringSearchAction  implements Serializable{

	/**
	 * 
	 */ 
	private static final long serialVersionUID = 8040153179287955265L;
	
	
	SchoolMonitoringSearchData schoolSearch;
	List<SetupData>activityList;
	List<SchoolMonitoringData> schoolExportList;
	LazySchoolMonitoringDataModel schoolDataModel;
	int role = 0;
	
	public int getRole() {
		return role;
	}
	public void setRole(int role) {
		this.role = role;
	}
	public SchoolMonitoringSearchData getSchoolSearch() {
		return schoolSearch;
	}
	public void setSchoolSearch(SchoolMonitoringSearchData schoolSearch) {
		this.schoolSearch = schoolSearch;
	}
	public List<SetupData> getActivityList() {
		return activityList;
	}
	public void setActivityList(ArrayList<SetupData> activityList) {
		this.activityList = activityList;
	}
	public LazySchoolMonitoringDataModel getSchoolDataModel() {
		return schoolDataModel;
	}
	public void setSchoolDataModel(LazySchoolMonitoringDataModel schoolDataModel) {
		this.schoolDataModel = schoolDataModel;
	}
	
	public List<SetupData> getActivityData()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		for(CommonEnum.ActivityStatus r : CommonEnum.ActivityStatus.values())
		{
			SetupData setup = new SetupData();
			setup.setLabel(r.description());
			setup.setValue(r.value());
			res.add(setup);
		}
		
		return res;
	}
	
	public void reset(String targetDataTable) {
		resetPagination(targetDataTable);
		loadData();
	}
	
	private void resetPagination(String targetDataTable) {
		DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent("schoolSearchForm:schoolSearchTable");
		d.setFirst(0);
	}
	
	public void search(String targetDataTable) {
			resetPagination(targetDataTable);
			schoolDataModel = new LazySchoolMonitoringDataModel(schoolSearch);	
	}
	
	@PostConstruct
	public void loadData()
	{
		HttpSession session = SessionUtil.getSession();
		if( session.getAttribute("userid") != null && session.getAttribute("role") != null)
		{	
			role = (int) session.getAttribute("role");
			activityList = getActivityData();
			schoolSearch = new SchoolMonitoringSearchData();
			schoolDataModel = new LazySchoolMonitoringDataModel(schoolSearch);
		}else
		{
				try {
					FacesContext.getCurrentInstance().getExternalContext().redirect("../../login.xhtml");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}
	
	
	public String edit(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("sc_monitoring_id", id);
		return "schoolMonitoringUpdate";
	}

	/**
	 * Put the userId as parameter. Go to user delete screen.
	 * 
	 * @param userId
	 * @return action outcome
	 */
	public String delete(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("sc_monitoring_id", id);
		return "schoolMonitoringDelete";
	}
	
	public String detail(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("sc_monitoring_id", id);
		return "schoolMonitoringDetail";
	}
	
	public String history(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("sc_monitoring_id", id);
		return "schoolMonitoringHistory";
	}
	
	/*public void postProcessXLS(Object document) 
	{
		
		HSSFWorkbook wb = (HSSFWorkbook) document;
		HSSFSheet sheet = wb.getSheetAt(0);
		HSSFCellStyle styleHeader = (HSSFCellStyle) wb.createCellStyle();
        HSSFFont fontHeader = (HSSFFont) wb.createFont();
        fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        styleHeader.setFont(fontHeader);
    
        for (Row row : sheet) {
            for (Cell cell : row) {
            	if(cell.getColumnIndex()!=5)
            	{
            		if(cell.getRowIndex()==0)
            			cell.setCellStyle(styleHeader);
            		cell.setCellValue(cell.getStringCellValue());	
            	}else cell.setCellValue("");               
            }
        }
	}*/
	
	public String  exportExcel() throws IOException
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		schoolSearch.setOffset(0);
		schoolSearch.setLimit(0);
		
		SchoolMonitoringPaginateData resData = SchoolMonitoringSearchMgr.findSchools(schoolSearch);
		SchoolMonitoringExcelAction.exportExcel(facesContext.getExternalContext(), resData.getSchoolList());
		  facesContext.responseComplete();
          facesContext.renderResponse();
		return null;
			
	}


}
