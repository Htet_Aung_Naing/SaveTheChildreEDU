package com.stc.action.monitoring;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.faces.context.ExternalContext;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.primefaces.util.Constants;

import com.stc.model.monitoring.SchoolMonitoringData;
import com.stc.model.monitoring.SchoolSupportData;
import com.stc.util.CommonEnum;
import com.stc.util.SurveyData;

public class SchoolMonitoringExcelAction {
	
	public static void exportExcel(ExternalContext context,List<SchoolMonitoringData> list) throws IOException
	{
		HSSFWorkbook workbook = new HSSFWorkbook();
		
		HSSFCellStyle styleHeader = (HSSFCellStyle) workbook.createCellStyle();
        HSSFFont fontHeader = (HSSFFont) workbook.createFont();
        fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        styleHeader.setFont(fontHeader);
		
		HSSFSheet sheet = workbook.createSheet("sheet 1");
		HSSFRow row = sheet.createRow(0); 
		createHeader(row, styleHeader);
		addContent(list, sheet);
		writeExcelToResponse(context, workbook, "schoolList"+new Date().getTime());
	
	}
	
	
	 public static void writeExcelToResponse(ExternalContext externalContext, Workbook generatedExcel, String filename) throws IOException {
	    	externalContext.setResponseContentType("application/vnd.ms-excel");
	    	externalContext.setResponseHeader("Expires", "0");
	    	externalContext.setResponseHeader("Cache-Control","must-revalidate, post-check=0, pre-check=0");
	    	externalContext.setResponseHeader("Pragma", "public");
	    	externalContext.setResponseHeader("Content-disposition", getContentDisposition(filename));
	    	externalContext.addResponseCookie(Constants.DOWNLOAD_COOKIE, "true", Collections.<String, Object>emptyMap());

	        OutputStream out = externalContext.getResponseOutputStream();
	        generatedExcel.write(out);
	        out.close();
	        externalContext.responseFlushBuffer();        
	       
	       
	    }

	    public static String getContentDisposition(String filename) {
	        return "attachment;filename="+ filename + ".xls";
	    }

	public static void createHeader(HSSFRow row,HSSFCellStyle headerStyle)
	{
		int i = 0;
		HSSFCell cell = row.createCell(i++);
		cell.setCellValue("School Code");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("School Township");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Type Of Activity");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Name Of The Village");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Date of Monitoring");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Established Year");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Size of building(L'xW'xH')");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Space/Size of school compound(L'xW')");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("No. of Latrine");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Availability of Clean Water");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Distance between ECCD service and Town");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Distance between ECCD service and School");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Frequency of MC Meeting");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Amount of revolving fund");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Monthly profits from revolving fund");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Monthly Fees from parents");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Other income");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Total Monthly income");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Total Caregiver salary");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("General Expense");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Total Expense");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Monthly Balance");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Issues/Problem");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Has been completely revolved");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Open/Close");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(i++);
		cell.setCellValue("Score of ECCD/APE mnimum standard");
		cell.setCellStyle(headerStyle);
		
		for(CommonEnum.SupportTitle r : CommonEnum.SupportTitle.values())
		{
			cell = row.createCell(i++);
			cell.setCellValue(r.description());
			cell.setCellStyle(headerStyle);
		}
		
		for(CommonEnum.SchoolMonitoringSurveyData r : CommonEnum.SchoolMonitoringSurveyData.values())
		{
			cell = row.createCell(i++);
			cell.setCellValue(r.description()+"(M)");
			cell.setCellStyle(headerStyle);
			cell = row.createCell(i++);
			cell.setCellValue(r.description()+"(F)");
			cell.setCellStyle(headerStyle);
			cell = row.createCell(i++);
			cell.setCellValue(r.description()+"(T)");
			cell.setCellStyle(headerStyle);
			cell = row.createCell(i++);
			cell.setCellValue(r.description()+" Description");
			cell.setCellStyle(headerStyle);
		}
	}
	
	public static void addContent(List<SchoolMonitoringData>schoolList,HSSFSheet sheet)
	{
		int row = 1;
		int column = 0;
		for (SchoolMonitoringData schoolMonitoringData : schoolList) 
		{
			HSSFRow rowexcel = sheet.createRow(row++);
			HSSFCell cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getSchoolCode());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getTowonship());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getActivityLabel());	
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getVillagName());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getMoniDate());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getEstabalDate());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getBuildingsize());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getCompundSize());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getNoOfToilet());
			cell = rowexcel.createCell(column++);
			cell.setCellValue((schoolMonitoringData.getGetWater() == CommonEnum.getCleanWater.Yes.value()) ? CommonEnum.getCleanWater.Yes.description():CommonEnum.getCleanWater.No.description());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getEccdServiceTownDistancelbl());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getEccdServiceSchoolDistancelbl());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getFrequencyMeetinglbl());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getAmountrevolvingfund());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getMonthlyRevolvingFund());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getFeeFromParent());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getOtherIncome());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getTotalMonthlyIncome());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getCaregiverSalary());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getGeneralExpense());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getCaregiverSalary() + schoolMonitoringData.getGeneralExpense());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getTotalMonthlyIncome() - schoolMonitoringData.getTotalexpense());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getIssue());
			cell = rowexcel.createCell(column++);
			cell.setCellValue((schoolMonitoringData.getCompleteResolve() == CommonEnum.rdoResolveComplete.Yes.value()) ? CommonEnum.rdoResolveComplete.Yes.description():CommonEnum.rdoResolveComplete.No.description());
			cell = rowexcel.createCell(column++);
			cell.setCellValue((schoolMonitoringData.getOpenClose() == CommonEnum.SchoolOpenClose.open.value()) ? CommonEnum.SchoolOpenClose.open.description():CommonEnum.SchoolOpenClose.close.description());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolMonitoringData.getMinScoreOfEccdApe());
			
			for (SchoolSupportData support : schoolMonitoringData.getSupportList()) 
			{
				cell = rowexcel.createCell(column++);
				cell.setCellValue(support.getSupporter_name());
			}
			
			for (SurveyData survey : schoolMonitoringData.getSurveyList()) 
			{
				cell = rowexcel.createCell(column++);
				cell.setCellValue(survey.getMalecount());
				cell = rowexcel.createCell(column++);
				cell.setCellValue(survey.getFemalecount());
				cell = rowexcel.createCell(column++);
				cell.setCellValue(survey.getMalecount()+survey.getFemalecount());
				cell = rowexcel.createCell(column++);
				cell.setCellValue(survey.getDescription());
			}
			
			column = 0;
		}
	}
	

}
