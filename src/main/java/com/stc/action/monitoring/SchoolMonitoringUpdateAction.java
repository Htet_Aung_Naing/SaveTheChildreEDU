package com.stc.action.monitoring;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.mgr.monitoring.SchoolMonitoringMgr;
import com.stc.mgr.monitoring.SchoolMonitoringSearchMgr;
import com.stc.model.monitoring.SchoolMonitoringData;
import com.stc.model.monitoring.SchoolSupportData;
import com.stc.util.CommonEnum;
import com.stc.util.SessionUtil;
import com.stc.util.SetupData;

@ManagedBean(name = "schoolMonitoringUpdateAction")
@ViewScoped
public class SchoolMonitoringUpdateAction implements Serializable{
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1657702281272573492L;
	String schoolcode;
	SchoolMonitoringData school;
	List<SetupData>activityList;
	List<SetupData> meetingList;
	public SchoolSupportData getSchoolSupporttech() {
		return schoolSupporttech;
	}
	public void setSchoolSupporttech(SchoolSupportData schoolSupporttech) {
		this.schoolSupporttech = schoolSupporttech;
	}
	public SchoolSupportData getSchoolSupportfund() {
		return schoolSupportfund;
	}
	public void setSchoolSupportfund(SchoolSupportData schoolSupportfund) {
		this.schoolSupportfund = schoolSupportfund;
	}
	public SchoolSupportData getSchoolSupporttlms() {
		return schoolSupporttlms;
	}
	public void setSchoolSupporttlms(SchoolSupportData schoolSupporttlms) {
		this.schoolSupporttlms = schoolSupporttlms;
	}

	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}

	SchoolSupportData schoolSupporttech;
	SchoolSupportData schoolSupportfund;
	SchoolSupportData schoolSupporttlms;
	String score;
	
	List<SetupData> distanceServiceTownList;
	List<SetupData> distanceServiceSchoolList;
	public String getSchoolcode() {
		return schoolcode;
	}
	public void setSchoolcode(String schoolcode) {
		this.schoolcode = schoolcode;
	}

	public SchoolMonitoringData getschool() {
		return school;
	}
	public void setschool(SchoolMonitoringData school) {
		this.school = school;
	}
	public List<SetupData> getActivityList() {
		return activityList;
	}
	public void setActivityList(List<SetupData> activityList) {
		this.activityList = activityList;
	}
	
	public List<SetupData> getMeetingData()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		for(CommonEnum.FrequencyMeeting g : CommonEnum.FrequencyMeeting.values())
		{
			SetupData setup = new SetupData();
			setup.setLabel(g.description());
			setup.setValue(g.value());
			res.add(setup);
		}
		
		return res;
	}
	
	public List<SetupData> getDistanceBtwServiceTownList()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		for(CommonEnum.DistanceEccdBtwTown g : CommonEnum.DistanceEccdBtwTown.values())
		{
			SetupData setup = new SetupData();
			setup.setLabel(g.description());
			setup.setValue(g.value());
			res.add(setup);
		}
		
		return res;
	}
	
	public List<SetupData> getDistanceServiceSchoolList()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		for(CommonEnum.DistanceEccdBtwSchool g : CommonEnum.DistanceEccdBtwSchool.values())
		{
			SetupData setup = new SetupData();
			setup.setLabel(g.description());
			setup.setValue(g.value());
			res.add(setup);
		}
		
		return res;
	}
	
	public List<SetupData> getActivityData()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		for(CommonEnum.ActivityStatus g : CommonEnum.ActivityStatus.values())
		{
			SetupData setup = new SetupData();
			setup.setLabel(g.description());
			setup.setValue(g.value());
			res.add(setup);
		}
		
		return res;
	}
	
	public ArrayList<SchoolSupportData> getSupportData()
	{
		ArrayList<SchoolSupportData>res = new ArrayList<SchoolSupportData>() ;
		for(CommonEnum.SupportTitle g : CommonEnum.SupportTitle.values())
		{
			SchoolSupportData support = new SchoolSupportData();
			support.setSupport_type(g.value());
			support.setSupport_lbl(g.description());
			
			res.add(support);
		}
		
		return res;
	}
	
	
	
	public SchoolMonitoringData getSchool() {
		return school;
	}
	public void setSchool(SchoolMonitoringData school) {
		this.school = school;
	}
	public List<SetupData> getMeetingList() {
		return meetingList;
	}
	public void setMeetingList(List<SetupData> meetingList) {
		this.meetingList = meetingList;
	}

	public List<SetupData> getDistanceServiceTownList() {
		return distanceServiceTownList;
	}
	public void setDistanceServiceTownList(List<SetupData> distanceServiceTownList) {
		this.distanceServiceTownList = distanceServiceTownList;
	}
	public void setDistanceServiceSchoolList(
			List<SetupData> distanceServiceSchoolList) {
		this.distanceServiceSchoolList = distanceServiceSchoolList;
	}
	@PostConstruct
	public void init()
	{
			
			school = new SchoolMonitoringData();
			activityList = getActivityData();
			distanceServiceSchoolList = getDistanceServiceSchoolList();
			distanceServiceTownList = getDistanceBtwServiceTownList();
			meetingList = getMeetingData();
			
			initSupporterData();
			if(schoolcode == null)
			{
				schoolcode = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("sc_monitoring_id");
				school = SchoolMonitoringMgr.findBycode(schoolcode);
			}else
			{
				school = SchoolMonitoringMgr.findBycode(schoolcode);
			}
			
			if(school != null)
			{
				for (SchoolSupportData support : school.getSupportList()) 
				{
					if(support.getSupport_type() == 1)
						schoolSupporttech = support;
					if(support.getSupport_type() == 2)
						schoolSupportfund = support;
					if(support.getSupport_type() == 3)
						schoolSupporttlms = support;
				}
			}
					
	}
	
	
	
	public void initSupporterData()
	{
		for(CommonEnum.SupportTitle g : CommonEnum.SupportTitle.values())
		{
			if(g.value() == 1)
			{
				schoolSupporttech = new SchoolSupportData();
				schoolSupporttech.setSupport_type(g.value());
				schoolSupporttech.setSupport_lbl(g.description());
				
			}
			if(g.value() == 2)
			{
				schoolSupportfund = new SchoolSupportData();
				schoolSupportfund.setSupport_type(g.value());
				schoolSupportfund.setSupport_lbl(g.description());
			}
			if(g.value() == 3)
			{
				schoolSupporttlms = new SchoolSupportData();
				schoolSupporttlms.setSupport_type(g.value());	
				schoolSupporttlms.setSupport_lbl(g.description());
			}
		}
	}
	
	public String update()
	{
		HttpSession session = SessionUtil.getSession();
		String userid = (String) session.getAttribute("userid");
		school.setCreatedUsername(userid);
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		school.setModifiedUsername(userid);
		school.setSupportList(new ArrayList<SchoolSupportData>());
		school.getSupportList().add(schoolSupporttech);
		school.getSupportList().add(schoolSupportfund);
		school.getSupportList().add(schoolSupporttlms);
		if(SchoolMonitoringSearchMgr.isValidUpdate(school.getSchoolCode(),school.getId()))	
		{
			if(SchoolMonitoringMgr.update(school, "SCHOOL_MONITORING_SURVEY","SCHOOL_MONITORING_HISTORY"))
			{
				school.setActivityLabel((school.getActivityType()==1)?CommonEnum.ActivityStatus.eccd.description():CommonEnum.ActivityStatus.ape.description());
				school.setMoniDate(df.format(school.getMonitoringDate()));
				school.setEstabalDate(df.format(school.getEstablishDate()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("schoolMonitoringData", school);
				return "monitoringschoolUpdateSuccess";
			}else
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Cannot Update School Data!"));
				return null;
			}
		}else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "School code must not be same"));
			return null;
		}
			
	}
	
	public void handleKeyEvent()
	{
		if(!school.getMinScoreOfEccdApe().equals(""))
		{
			if(Integer.parseInt(school.getMinScoreOfEccdApe()) > 10 && Integer.parseInt(school.getMinScoreOfEccdApe())<30)
			{
				score = "normal";
			}
			else if(Integer.parseInt(school.getMinScoreOfEccdApe()) > 30 && Integer.parseInt(school.getMinScoreOfEccdApe())<50)
			{
				score="high";
			}
			else
			{
				score="";
			}
		}else
		{
			score="";
		}
	}

}
