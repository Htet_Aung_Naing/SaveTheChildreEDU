package com.stc.action.monitoring;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.mgr.monitoring.SchoolMonitoringMgr;
import com.stc.model.monitoring.SchoolMonitoringData;

@ManagedBean(name = "schoolMonitoringDeleteAction")
@ViewScoped
public class SchoolMonitoringDeleteAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1480308834338808465L;
	
	String schoolId;
	SchoolMonitoringData school;
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public SchoolMonitoringData getSchool() {
		return school;
	}
	public void setSchool(SchoolMonitoringData school) {
		this.school = school;
	}
	
	@PostConstruct
	public void init()
	{
	
			school = new SchoolMonitoringData();
			if(schoolId == null)
			{
				this.schoolId = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("sc_monitoring_id");
				this.school = SchoolMonitoringMgr.findBycode(schoolId);
			}
		
	}
	
	public String delete()
	{
		if(SchoolMonitoringMgr.delete("school_monitoring_survey","SCHOOL_monitoring_history",school))
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Delete School Successfully!"));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			return "schoolMonitoringSearch";
		}else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "School cannot delete!"));
			return null;
		}

	}

}
