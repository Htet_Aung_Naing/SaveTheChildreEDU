package com.stc.action.updating;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.mgr.updating.SchoolSearchMgr;
import com.stc.mgr.updating.SchoolUpdatingMgr;
import com.stc.model.updating.SchoolUpdatingData;

@ManagedBean(name = "schoolDeleteAction")
@ViewScoped
public class SchoolUpdatingDeleteAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1480308834338808465L;
	
	String schoolId;
	SchoolUpdatingData school;
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public SchoolUpdatingData getSchool() {
		return school;
	}
	public void setSchool(SchoolUpdatingData school) {
		this.school = school;
	}
	
	@PostConstruct
	public void init()
	{
	
		
			school = new SchoolUpdatingData();
			if(schoolId == null)
			{
				this.schoolId = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("scid");
				this.school = SchoolSearchMgr.findBycode(schoolId);
			}
		
	}
	
	public String delete()
	{
		if(SchoolUpdatingMgr.delete("SCHOOL_UPDATING_SURVEY","SCHOOL_UPDATING_HISTORY",school))
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Delete School Successfully!"));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			return "schoolUpdateSearch";
		}else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "School cannot delete!"));
			return null;
		}

	}

}
