package com.stc.action.updating;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.model.updating.SchoolUpdatingData;
import com.stc.util.CommonEnum;

@ManagedBean(name = "schoolUpadteCompleteAction")
@ViewScoped
public class SchoolUpdateCompleteAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6689140211863682938L;
	
	SchoolUpdatingData school;

	public SchoolUpdatingData getSchool() {
		return school;
	}

	public void setSchool(SchoolUpdatingData school) {
		this.school = school;
	}
	
	@PostConstruct
	public void init()
	{

			this.school = (SchoolUpdatingData) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("schoolUpdatData");
			school.setActivityLabel(((school.getActivityType()==1)?CommonEnum.ActivityStatus.eccd.description():CommonEnum.ActivityStatus.ape.description()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		
	}
	
}
