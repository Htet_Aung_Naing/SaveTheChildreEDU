package com.stc.action.updating;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.stc.mgr.updating.SchoolSearchMgr;
import com.stc.model.updating.SchoolUpdatingData;
import com.stc.util.CommonEnum;
import com.stc.util.SurveyData;

@ManagedBean(name = "schoolDetailAction")
@ViewScoped
public class SchoolDetailAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6689140211863682938L;
	
	SchoolUpdatingData school;
	String sc_code;

	
	public String getSc_code() {
		return sc_code;
	}

	public void setSc_code(String sc_code) {
		this.sc_code = sc_code;
	}

	public SchoolUpdatingData getSchool() {
		return school;
	}

	public void setSchool(SchoolUpdatingData school) {
		this.school = school;
	}
	
	public void setActivityLabel(SchoolUpdatingData school)
	{
		for (SurveyData survey : school.getSchoolSurveyList()) 
		{
			for(CommonEnum.SchoolSurveyRequirement g : CommonEnum.SchoolSurveyRequirement.values())
			{
				if(survey.getTypeId() == g.value())
				{
					survey.setLabel(g.description());
					break;
				}
			}
		}
		
	}
	
	@PostConstruct
	public void init()
	{
	
			school = new SchoolUpdatingData();
			
			if(sc_code==null)
			{
				sc_code = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("scid");
				school = SchoolSearchMgr.findBycode(sc_code);
				school.setActivityLabel((school.getActivityType()==1)?CommonEnum.ActivityStatus.eccd.description():CommonEnum.ActivityStatus.ape.description());
				
				//setActivityLabel(school);
			}
				
	}
	
}
