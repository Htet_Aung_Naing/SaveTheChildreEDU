package com.stc.action.updating;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.component.datatable.DataTable;

import com.stc.lazyDataModel.schoolUpdate.LazySchoolUpdateHitoryModel;
import com.stc.mgr.updating.SchoolSearchMgr;
import com.stc.model.updating.SchoolHistorySearchData;
import com.stc.model.updating.SchoolUpdatingData;
import com.stc.util.CommonEnum;
import com.stc.util.SessionUtil;

@ManagedBean(name = "schoolUpdatingHistoryAction")
@ViewScoped
public class SchoolUpdatingHsitoryAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8040153179287955265L;
	
	
	LazySchoolUpdateHitoryModel historyDatamodel;
	SchoolHistorySearchData historysearch;
	String sc_code ;
	SchoolUpdatingData school;
	
	public SchoolHistorySearchData getHistorysearch() {
		return historysearch;
	}

	public void setHistorysearch(SchoolHistorySearchData historysearch) {
		this.historysearch = historysearch;
	}

	public String getSc_code() {
		return sc_code;
	}

	public void setSc_code(String sc_code) {
		this.sc_code = sc_code;
	}

	public SchoolUpdatingData getSchool() {
		return school;
	}

	public void setSchool(SchoolUpdatingData school) {
		this.school = school;
	}

	public LazySchoolUpdateHitoryModel getHistoryDatamodel() {
		return historyDatamodel;
	}

	public void setHistoryDatamodel(LazySchoolUpdateHitoryModel historyDatamodel) {
		this.historyDatamodel = historyDatamodel;
	}

	
	public void reset(String targetDataTable) {
		resetPagination(targetDataTable);
		loadData();
	}
	
	private void resetPagination(String targetDataTable) {
		DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent("schoolHistoryform:schoolHistoryTable");
		d.setFirst(0);
	}
	

	
	@PostConstruct
	public void loadData()
	{

		
			school = new SchoolUpdatingData();
			historysearch = new SchoolHistorySearchData();
			
			if(sc_code==null)
			{
				sc_code = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("scid");
				school = SchoolSearchMgr.findBycode(sc_code);
				school.setActivityLabel((school.getActivityType()==1)?CommonEnum.ActivityStatus.eccd.description():CommonEnum.ActivityStatus.ape.description());
				historysearch.setSccode(sc_code);
				historyDatamodel = new LazySchoolUpdateHitoryModel(historysearch,"school_updating_history");
	
			}
		
	}
	
	
	public String edit(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("scid", id);
		return "schoolSurveyUpdate";
	}

}
