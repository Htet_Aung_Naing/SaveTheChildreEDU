package com.stc.action.updating;


import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.primefaces.component.datatable.DataTable;

import com.stc.lazyDataModel.schoolUpdate.LazySchoolUpdateDataModel;
import com.stc.mgr.updating.SchoolSearchMgr;
import com.stc.model.updating.SchoolSearchData;
import com.stc.model.updating.SchoolUpdatePaginateData;
import com.stc.model.updating.SchoolUpdatingData;
import com.stc.util.CommonEnum;
import com.stc.util.SessionUtil;
import com.stc.util.SetupData;
import com.stc.util.SurveyData;

@ManagedBean(name = "schoolUpdatingSearchAction")
@ViewScoped
public class SchoolUpdatingSearchAction  implements Serializable{

	/**
	 * 
	 */ 
	private static final long serialVersionUID = 8040153179287955265L;
	
	
	SchoolSearchData schoolSearch;
	List<SetupData>activityList;
	LazySchoolUpdateDataModel schoolDataModel;
	int role;
	public int getRole() {
		return role;
	}
	public void setRole(int role) {
		this.role = role;
	}
	public SchoolSearchData getSchoolSearch() {
		return schoolSearch;
	}
	public void setSchoolSearch(SchoolSearchData schoolSearch) {
		this.schoolSearch = schoolSearch;
	}
	public List<SetupData> getActivityList() {
		return activityList;
	}
	public void setActivityList(ArrayList<SetupData> activityList) {
		this.activityList = activityList;
	}
	public LazySchoolUpdateDataModel getSchoolDataModel() {
		return schoolDataModel;
	}
	public void setSchoolDataModel(LazySchoolUpdateDataModel schoolDataModel) {
		this.schoolDataModel = schoolDataModel;
	}
	
	public List<SetupData> getActivityData()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		for(CommonEnum.ActivityStatus r : CommonEnum.ActivityStatus.values())
		{
			SetupData setup = new SetupData();
			setup.setLabel(r.description());
			setup.setValue(r.value());
			res.add(setup);
		}
		
		return res;
	}
	
	public void reset(String targetDataTable) {
		resetPagination(targetDataTable);
		loadData();
	}
	
	private void resetPagination(String targetDataTable) {
		DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent("schoolSearchForm:schoolSearchTable");
		d.setFirst(0);
	}
	
	public void search(String targetDataTable) {
			resetPagination(targetDataTable);
			schoolDataModel = new LazySchoolUpdateDataModel(schoolSearch);
		
	}
	
	@PostConstruct
	public void loadData()
	{
		
		HttpSession session = SessionUtil.getSession();
		if( session.getAttribute("userid") != null && session.getAttribute("role") != null)
		{
			role = (int) session.getAttribute("role");
			activityList = getActivityData();
			schoolSearch = new SchoolSearchData();
			schoolDataModel = new LazySchoolUpdateDataModel(schoolSearch);
		}else
		{
				try {
					FacesContext.getCurrentInstance().getExternalContext().redirect("../../login.xhtml");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}
	
	
	public String edit(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("scid", id);
		return "schoolSurveyUpdate";
	}

	/**
	 * Put the userId as parameter. Go to user delete screen.
	 * 
	 * @param userId
	 * @return action outcome
	 */
	public String delete(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("scid", id);
		return "schoolDelete";
	}
	
	public String detail(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("scid", id);
		return "schoolUpdatingDetail";
	}
	
	public String history(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("scid", id);
		return "SchoolUpdatingHistory";
	}
	
	/*public void postProcessXLS(Object document) 
	{
		
		HSSFWorkbook wb = (HSSFWorkbook) document;
		HSSFSheet sheet = wb.getSheetAt(0);
		HSSFCellStyle styleHeader = (HSSFCellStyle) wb.createCellStyle();
        HSSFFont fontHeader = (HSSFFont) wb.createFont();
        fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        styleHeader.setFont(fontHeader);
    
        for (Row row : sheet) {
            for (Cell cell : row) {
            	if(cell.getColumnIndex()!=5)
            	{
            		if(cell.getRowIndex()==0)
            			cell.setCellStyle(styleHeader);
            		cell.setCellValue(cell.getStringCellValue());	
            	}else cell.setCellValue("");               
            }
        }
	}*/
	
	public String  exportExcel() throws IOException
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		schoolSearch.setOffset(0);
		schoolSearch.setLimit(0);
		
		SchoolUpdatePaginateData resData = SchoolSearchMgr.findSchools(schoolSearch);
		
		SchoolUpdatingExcelAction.exportExcel(facesContext.getExternalContext(), resData.getSchoolList(), activityList);
		  facesContext.responseComplete();
          facesContext.renderResponse();
	  
		
	/*	try {
			
			File l_file = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/")+"\\schoolUpdatingExcel\\"+"schoolList.xls");
			boolean flag = l_file.createNewFile();
			WorkbookSettings wbSettings = new WorkbookSettings();
			wbSettings.setLocale(new Locale("en", "EN"));
			WritableWorkbook workbook = Workbook.createWorkbook(l_file, wbSettings);
			workbook.createSheet("List", 0);
			
			WritableSheet excelSheet1 = workbook.getSheet(0);
			addHeader(excelSheet1);
			addContent(excelSheet1);
			workbook.write();
			workbook.close();
			
		
			
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		return null;
			
	}
	
	 
	    
	
	public void addHeader(WritableSheet asheet) throws WriteException
	{
		 WritableCellFormat timesBoldUnderline;
		WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD, false);
		timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
		// Lets automatically wrap the cells
		timesBoldUnderline.setWrap(true);
		Label label;
		label = new Label(0, 0, "School Code", timesBoldUnderline);
		asheet.addCell(label);
		label = new Label(1,0,"School Township" ,timesBoldUnderline );
		asheet.addCell(label);
		label = new  Label(2,0,"Type Of Activity",timesBoldUnderline);
		asheet.addCell(label);
		label = new Label(3,0,"Name Of The Village" , timesBoldUnderline);
		asheet.addCell(label);
		label = new Label(4 , 0 , "Date Of Monitoring" , timesBoldUnderline);
		int i = 4;
		for (SetupData setupData : activityList) 
		{
			label = new Label(++i,0,setupData.getLabel()+"(Male)",timesBoldUnderline);
			asheet.addCell(label);
			label = new Label(++i,0,setupData.getLabel()+"(Female)",timesBoldUnderline);
			asheet.addCell(label);
			label = new Label(++i,0,setupData.getLabel()+"(Total)",timesBoldUnderline);
			asheet.addCell(label);
		}
		
	}
	
	public void addContent(WritableSheet asheet) throws RowsExceededException, WriteException
	{
		int column = 0 ; int row = 1 ;
		for (SchoolUpdatingData school : schoolDataModel.getSchoolList()) 
		{
			Label label = new Label(column++ , row , school.getSchoolCode());
			asheet.addCell(label);
			label = new Label(column++ , row , school.getTowonship());
			asheet.addCell(label);
			label = new Label(column++ , row , school.getActivityLabel());
			asheet.addCell(label);
			label = new Label(column++ , row , school.getVillagName());
			asheet.addCell(label);
			label = new  Label(column++ , row , school.getMoniDate());
			asheet.addCell(label);
			for (SurveyData survey : school.getSchoolSurveyList()) 
			{
				label = new Label(column++ , row , String.valueOf(survey.getMalecount()));
				asheet.addCell(label);
				label = new Label(column++ , row , String.valueOf(survey.getFemalecount()));
				asheet.addCell(label);
				label = new Label(column++ , row , String.valueOf(survey.getFemalecount()+survey.getMalecount()));
				asheet.addCell(label);
			}
		}
	}
	
	

}
