package com.stc.action.updating;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.faces.context.ExternalContext;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.primefaces.util.Constants;

import com.stc.model.updating.SchoolUpdatingData;
import com.stc.util.CommonEnum;
import com.stc.util.SetupData;
import com.stc.util.SurveyData;

public class SchoolUpdatingExcelAction {
	
	public static void exportExcel(ExternalContext context,List<SchoolUpdatingData> list,List<SetupData> activityList) throws IOException
	{
		HSSFWorkbook workbook = new HSSFWorkbook();
		
		HSSFCellStyle styleHeader = (HSSFCellStyle) workbook.createCellStyle();
        HSSFFont fontHeader = (HSSFFont) workbook.createFont();
        fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        styleHeader.setFont(fontHeader);
		
		HSSFSheet sheet = workbook.createSheet("sheet 1");
		HSSFRow row = sheet.createRow(0); 
		createHeader(row, activityList,styleHeader);
		addContent(list, sheet);
		writeExcelToResponse(context, workbook, "schoolList"+new Date().getTime());
	
	}
	
	
	 public static void writeExcelToResponse(ExternalContext externalContext, Workbook generatedExcel, String filename) throws IOException {
	    	externalContext.setResponseContentType("application/vnd.ms-excel");
	    	externalContext.setResponseHeader("Expires", "0");
	    	externalContext.setResponseHeader("Cache-Control","must-revalidate, post-check=0, pre-check=0");
	    	externalContext.setResponseHeader("Pragma", "public");
	    	externalContext.setResponseHeader("Content-disposition", getContentDisposition(filename));
	    	externalContext.addResponseCookie(Constants.DOWNLOAD_COOKIE, "true", Collections.<String, Object>emptyMap());

	        OutputStream out = externalContext.getResponseOutputStream();
	        generatedExcel.write(out);
	        out.close();
	        externalContext.responseFlushBuffer();        
	       
	       
	    }

	    public static String getContentDisposition(String filename) {
	        return "attachment;filename="+ filename + ".xls";
	    }

	public static void createHeader(HSSFRow row,List<SetupData>setupList,HSSFCellStyle headerStyle)
	{
		HSSFCell cell = row.createCell(0);
		cell.setCellValue("School Code");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(1);
		cell.setCellValue("School Township");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(2);
		cell.setCellValue("Type Of Activity");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(3);
		cell.setCellValue("Name Of The Village");
		cell.setCellStyle(headerStyle);
		cell = row.createCell(4);
		cell.setCellValue("Date of Monitoring");
		cell.setCellStyle(headerStyle);
		int i = 5;
		for(CommonEnum.SchoolSurveyRequirement r : CommonEnum.SchoolSurveyRequirement.values())
		{
			cell = row.createCell(i++);
			cell.setCellValue(r.description()+"(M)");
			cell.setCellStyle(headerStyle);
			cell = row.createCell(i++);
			cell.setCellValue(r.description()+"(F)");
			cell.setCellStyle(headerStyle);
			cell = row.createCell(i++);
			cell.setCellValue(r.description()+"(T)");
			cell.setCellStyle(headerStyle);
			cell = row.createCell(i++);
			cell.setCellValue(r.description()+" Description");
			cell.setCellStyle(headerStyle);
		}
	}
	
	public static void addContent(List<SchoolUpdatingData>schoolList,HSSFSheet sheet)
	{
		int row = 1;
		int column = 0;
		for (SchoolUpdatingData schoolUpdatingData : schoolList) 
		{
			HSSFRow rowexcel = sheet.createRow(row++);
			HSSFCell cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolUpdatingData.getSchoolCode());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolUpdatingData.getTowonship());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolUpdatingData.getActivityLabel());	
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolUpdatingData.getVillagName());
			cell = rowexcel.createCell(column++);
			cell.setCellValue(schoolUpdatingData.getMoniDate());
			for (SurveyData survey : schoolUpdatingData.getSchoolSurveyList()) 
			{
				cell = rowexcel.createCell(column++);
				cell.setCellValue(survey.getMalecount());
				cell = rowexcel.createCell(column++);
				cell.setCellValue(survey.getFemalecount());
				cell = rowexcel.createCell(column++);
				cell.setCellValue(survey.getMalecount()+survey.getFemalecount());
				cell = rowexcel.createCell(column++);
				cell.setCellValue(survey.getDescription());
			}
			
			column = 0;
		}
	}
	
	public static void main(String[]args)
	{
		
	}
	
}
