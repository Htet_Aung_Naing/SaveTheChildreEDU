package com.stc.action.updating;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.mgr.updating.SchoolSearchMgr;
import com.stc.mgr.updating.SchoolUpdatingMgr;
import com.stc.model.updating.SchoolUpdatingData;
import com.stc.util.CommonEnum;
import com.stc.util.SessionUtil;
import com.stc.util.SetupData;
import com.stc.util.SurveyData;

@ManagedBean(name = "schoolUpdateAction")
@ViewScoped
public class SchoolSurveyUpdateAction implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1123969045861755611L;
	String schoolcode;
	SchoolUpdatingData schoolData;
	List<SetupData>activityList;
	public String getSchoolcode() {
		return schoolcode;
	}
	public void setSchoolcode(String schoolcode) {
		this.schoolcode = schoolcode;
	}

	public SchoolUpdatingData getSchoolData() {
		return schoolData;
	}
	public void setSchoolData(SchoolUpdatingData schoolData) {
		this.schoolData = schoolData;
	}
	public List<SetupData> getActivityList() {
		return activityList;
	}
	public void setActivityList(List<SetupData> activityList) {
		this.activityList = activityList;
	}
	
	public List<SetupData> getActivityData()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		for(CommonEnum.ActivityStatus g : CommonEnum.ActivityStatus.values())
		{
			SetupData setup = new SetupData();
			setup.setLabel(g.description());
			setup.setValue(g.value());
			res.add(setup);
		}
		
		return res;
	}
	
	@PostConstruct
	public void init()
	{
		HttpSession session = SessionUtil.getSession();
		if( session.getAttribute("userid") != null && session.getAttribute("role") != null)
		{
			schoolData = new SchoolUpdatingData();
			activityList = getActivityData();
			if(schoolcode == null)
			{
				schoolcode = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("scid");
				schoolData = SchoolSearchMgr.findBycode(schoolcode);
			}else
			{
				schoolData = SchoolSearchMgr.findBycode(schoolcode);
			}
		}else
		{
				try {
					FacesContext.getCurrentInstance().getExternalContext().redirect("../../login.xhtml");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
			
	}
	
	public String validChildrenTotal(List<SurveyData> surveyList)
	{
		String res = "";
		
		if(surveyList.get(1).getMalecount() > surveyList.get(0).getMalecount())
			res = surveyList.get(1).getLabel()+" male count must be less than "+surveyList.get(0).getLabel()+" male count!";
		else if(surveyList.get(1).getFemalecount() > surveyList.get(0).getFemalecount())
			res = surveyList.get(1).getLabel()+" female count must be less than "+surveyList.get(0).getLabel()+" female count!";
		else if(surveyList.get(2).getMalecount() > surveyList.get(0).getMalecount())
			res = surveyList.get(2).getLabel()+" male count must be less than "+surveyList.get(0).getLabel()+" male count!";
		else if(surveyList.get(2).getFemalecount() > surveyList.get(0).getFemalecount())
			res = surveyList.get(2).getLabel()+" female count must be less than "+surveyList.get(0).getLabel()+" female count!";
		else if(surveyList.get(3).getMalecount() > surveyList.get(0).getMalecount())
			res = surveyList.get(3).getLabel()+" male count must be less than "+surveyList.get(0).getLabel()+" male count!";
		else if(surveyList.get(3).getFemalecount() > surveyList.get(0).getFemalecount())
			res = surveyList.get(3).getLabel()+" female count must be less than "+surveyList.get(0).getLabel()+" female count!";
		
		return res;
	}
	
	public String update()
	{
		HttpSession session = SessionUtil.getSession();
		String userid = (String) session.getAttribute("userid");
		schoolData.setCreatedUsername(userid);
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		schoolData.setModifiedUsername(userid);
		String validmessage = "";
		validmessage = validChildrenTotal(schoolData.getSchoolSurveyList());
		if(validmessage.equals(""))
		{
			if(SchoolSearchMgr.isValidUpdate(schoolData.getSchoolCode(),schoolData.getId()))	
			{
				if(SchoolUpdatingMgr.update(schoolData, "SCHOOL_UPDATING_SURVEY","SCHOOL_UPDATING_HISTORY"))
				{
					schoolData.setActivityLabel((schoolData.getActivityType()==1)?CommonEnum.ActivityStatus.eccd.description():CommonEnum.ActivityStatus.ape.description());
					schoolData.setMoniDate(df.format(schoolData.getMonitoringDate()));
					FacesContext.getCurrentInstance().getExternalContext().getFlash().put("schoolUpdatData", schoolData);
					return "schoolSurveyUpdateSuccess";
				}else
				{
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Cannot Update schoolData!"));
					return null;
				}
			}else
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "School code must not be same"));
				return null;
			}
		}else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", validmessage));
			return null;
		}
		
			
	}

}
