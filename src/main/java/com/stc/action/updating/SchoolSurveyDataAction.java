package com.stc.action.updating;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.stc.mgr.updating.SchoolSearchMgr;
import com.stc.mgr.updating.SchoolUpdatingMgr;
import com.stc.model.updating.SchoolUpdatingData;
import com.stc.util.CommonEnum;
import com.stc.util.SessionUtil;
import com.stc.util.SetupData;
import com.stc.util.SurveyData;

@ManagedBean(name = "schoolSurveyAction")
@ViewScoped
public class SchoolSurveyDataAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2041004147215897996L;
	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	SchoolUpdatingData school;
	List<SetupData> activityList;

	public List<SetupData> getActivityList() {
		return activityList;
	}

	public void setActivityList(List<SetupData> activityList) {
		this.activityList = activityList;
	}

	public SchoolUpdatingData getSchool() {
		return school;
	}

	public void setSchool(SchoolUpdatingData school) {
		this.school = school;
	}
	
	public ArrayList<SurveyData> getSurveyData()
	{
		ArrayList<SurveyData>res = new ArrayList<SurveyData>() ;
		for(CommonEnum.SchoolSurveyRequirement g : CommonEnum.SchoolSurveyRequirement.values())
		{
			SurveyData suv = new SurveyData();
			suv.setLabel(g.description());
			suv.setTypeId(g.value());
			res.add(suv);
		}
		
		return res;
	}
	
	public List<SetupData> getActivityData()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		for(CommonEnum.ActivityStatus g : CommonEnum.ActivityStatus.values())
		{
			SetupData setup = new SetupData();
			setup.setLabel(g.description());
			setup.setValue(g.value());
			res.add(setup);
		}
		
		return res;
	}
	
	@PostConstruct
	public void init()
	{
			this.school = new SchoolUpdatingData();
			this.activityList = getActivityData();
			this.school.schoolSurveyList = getSurveyData();
		
	}
	
	public String validChildrenTotal(List<SurveyData> surveyList)
	{
		String res = "";
		
		if(surveyList.get(1).getMalecount() > surveyList.get(0).getMalecount())
			res = surveyList.get(1).getLabel()+" male count must be less than "+surveyList.get(0).getLabel()+" male count!";
		else if(surveyList.get(1).getFemalecount() > surveyList.get(0).getFemalecount())
			res = surveyList.get(1).getLabel()+" female count must be less than "+surveyList.get(0).getLabel()+" female count!";
		else if(surveyList.get(2).getMalecount() > surveyList.get(0).getMalecount())
			res = surveyList.get(2).getLabel()+" male count must be less than "+surveyList.get(0).getLabel()+" male count!";
		else if(surveyList.get(2).getFemalecount() > surveyList.get(0).getFemalecount())
			res = surveyList.get(2).getLabel()+" female count must be less than "+surveyList.get(0).getLabel()+" female count!";
		else if(surveyList.get(3).getMalecount() > surveyList.get(0).getMalecount())
			res = surveyList.get(3).getLabel()+" male count must be less than "+surveyList.get(0).getLabel()+" male count!";
		else if(surveyList.get(3).getFemalecount() > surveyList.get(0).getFemalecount())
			res = surveyList.get(3).getLabel()+" female count must be less than "+surveyList.get(0).getLabel()+" female count!";
		
		return res;
	}
	
	public String save()
	{
		HttpSession session = SessionUtil.getSession();
		String userid = (String) session.getAttribute("userid");
		school.setCreatedUsername(userid);
		school.setModifiedUsername(userid);
		school.setRecordStatus(1);
		String validMessage = "";
		validMessage = validChildrenTotal(school.getSchoolSurveyList());
		
		if(validMessage.equals(""))
		{
			if(SchoolSearchMgr.isValid(school.getSchoolCode()))	
			{
				if(SchoolUpdatingMgr.insert(school))
				{
					school.setMoniDate(df.format(school.getMonitoringDate()));
					FacesContext.getCurrentInstance().getExternalContext().getFlash().put("schoolSurvey", school);
					return "schoolSurveySuccess";
				}else
				{
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Can't Save complete!"));
					return null;
				}
			}else 
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "School code must not be same"));
				return null;
			}
			
		}else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", validMessage));
			return null;
		}
	
			
	}
	
	

}
