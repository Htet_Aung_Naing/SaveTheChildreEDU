package com.stc.api;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.stc.dao.user.LoginDao;
import com.stc.mgr.monitoring.SchoolMonitoringMgr;
import com.stc.mgr.updating.SchoolSearchMgr;
import com.stc.mgr.updating.SchoolUpdatingMgr;
import com.stc.model.monitoring.SchoolMonitoringData;
import com.stc.model.monitoring.SchoolMonitoringListWrapper;
import com.stc.model.updating.SchoolUpdatingData;
import com.stc.model.updating.SchoolUpdatingListWrapper;
import com.stc.model.user.UserInfo;

@RestController
public class MobileApiController {
	
	@RequestMapping(value = "/userapi/login/{code}/{password}", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public  UserInfo getValidUser(@PathVariable("code") String code , @PathVariable("password") String password) throws JsonIOException, IOException
	{

		UserInfo user = new  UserInfo();
		user = LoginDao.validate(code , password);
	
		return user;	
	}
	
/*	@RequestMapping(value="/schoolapi/insert/" , method = RequestMethod.GET  )
	public String inserSchoolUpdatinList(@RequestParam String schoolWrapper ,  UriComponentsBuilder ucBuilder)
	{
		String res = "";
		for (SchoolUpdatingData schoolUpdatingData : schoolList) 
		{
			res += schoolUpdatingData.getSchoolCode()+",";
		}
		Gson gson = new Gson();
		List<SchoolUpdatingData> wrapper = (List<SchoolUpdatingData>) gson.fromJson(schoolWrapper, SchoolUpdatingData.class);
		
		return "res";
	}*/
	
	@RequestMapping(value="/schoolupdating/insert/" , method = RequestMethod.POST  )
	public ResponseData inserSchoolUpdatingList(@RequestBody String schoolList ,  UriComponentsBuilder ucBuilder)
	{
		ResponseData res = new ResponseData();
		Gson gson = new Gson();
		ArrayList<String> errorScCodeLst = new ArrayList<String>();
		ArrayList<String> saveScCodeLst = new ArrayList<String>();
		SchoolUpdatingListWrapper schoolUpadtingWrapper = gson.fromJson(schoolList, SchoolUpdatingListWrapper.class);
		for (SchoolUpdatingData school : schoolUpadtingWrapper.getSchoolLsit()) 
		{
			if(SchoolSearchMgr.isValid(school.getSchoolCode()))
			{
				school.setRecordStatus(1);
				if(!SchoolUpdatingMgr.insert(school))
				{
					res.setServerError(true);
					break;
				}				
				else
					saveScCodeLst.add(school.getSchoolCode());
			}
			else
			{
				if(SchoolUpdatingMgr.updateApi(school, "SCHOOL_UPDATING_SURVEY", "SCHOOL_UPDATING_HISTORY"))
				{
					saveScCodeLst.add(school.getSchoolCode());
				}else
				{
					errorScCodeLst.add(school.getSchoolCode());
				}
					
			}
		}
		res.setErrorScCodeList(errorScCodeLst);
		res.setSaveScCodeList(saveScCodeLst);
		return res;
	}

	
	@RequestMapping(value="/schoolmonitoring/insert/" , method = RequestMethod.POST  )
	public ResponseData inserSchoolMonitoringList(@RequestBody String schoolList ,  UriComponentsBuilder ucBuilder)
	{
		ResponseData res = new ResponseData();
		Gson gson = new Gson();
		ArrayList<String> errorScCodeLst = new ArrayList<String>();
		ArrayList<String> saveScCodeLst = new ArrayList<String>();
		SchoolMonitoringListWrapper schoolUpadtingWrapper = gson.fromJson(schoolList, SchoolMonitoringListWrapper.class);
		for (SchoolMonitoringData school : schoolUpadtingWrapper.getSchoolLsit()) 
		{
//		if(SchoolMonitoringSearchMgr.isValid(school.getSchoolCode()))
//			{
				school.setRecordStatus(1);
				if(!SchoolMonitoringMgr.insert(school))
				{
					res.setServerError(true);
					break;
				}				
				else
					saveScCodeLst.add(school.getSchoolCode());
			//}
//			else
//			{
//				school.setRecordStatus(1);
//				if(SchoolMonitoringMgr.updateFromApi(school, "SCHOOL_MONITORING_SURVEY", "SCHOOL_MONITORING_HISTORY"))
//				{
//					saveScCodeLst.add(school.getSchoolCode());
//				}else
//				{
//					errorScCodeLst.add(school.getSchoolCode());
//				}
//					
//			}
		}
		res.setErrorScCodeList(errorScCodeLst);
		res.setSaveScCodeList(saveScCodeLst);
		return res;
	}
	
	
}
