package com.stc.model.updating;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class SchoolHistoryShowData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5760437496315232884L;
	Date modifiedDate;
	ArrayList<SchoolHistoryData> historyLIst;
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public ArrayList<SchoolHistoryData> getHistoryLIst() {
		return historyLIst;
	}
	public void setHistoryLIst(ArrayList<SchoolHistoryData> historyLIst) {
		this.historyLIst = historyLIst;
	}
	
	public  SchoolHistoryShowData()
	{
		this.modifiedDate = new Date();
		this.historyLIst = new ArrayList<SchoolHistoryData>();
	}

}
