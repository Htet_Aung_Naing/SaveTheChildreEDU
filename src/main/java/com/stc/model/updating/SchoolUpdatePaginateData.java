package com.stc.model.updating;

import java.io.Serializable;
import java.util.List;

public class SchoolUpdatePaginateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7073929052523728817L;
	
	int count;
	List<SchoolUpdatingData> schoolList;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<SchoolUpdatingData> getSchoolList() {
		return schoolList;
	}
	public void setSchoolList(List<SchoolUpdatingData> schoolList) {
		this.schoolList = schoolList;
	}
	

}
