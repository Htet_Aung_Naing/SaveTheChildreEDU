package com.stc.model.updating;

import java.io.Serializable;
import java.util.List;

public class SchoolUpdateHistoryPaginateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7073929052523728817L;
	
	int count;
	List<SchoolHistoryData> schoolHistoryList;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<SchoolHistoryData> getSchoolHistoryList() {
		return schoolHistoryList;
	}
	public void setSchoolHistoryList(List<SchoolHistoryData> schoolHistoryList) {
		this.schoolHistoryList = schoolHistoryList;
	}


}
