package com.stc.model.updating;

import java.io.Serializable;
import java.util.List;

public class SchoolUpdatingListWrapper implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7187840551222772340L;
	
	List<SchoolUpdatingData> schoolLsit;

	public List<SchoolUpdatingData> getSchoolLsit() {
		return schoolLsit;
	}

	public void setSchoolLsit(List<SchoolUpdatingData> schoolLsit) {
		this.schoolLsit = schoolLsit;
	}

}
