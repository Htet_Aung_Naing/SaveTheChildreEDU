package com.stc.model.updating;

import java.io.Serializable;

public class SchoolSearchData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8439064383719560384L;
	
	String sccode;
	String township;
	int activity;
	String vilageName;
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}

	int offset;
	int limit;
	
	public String getSccode() {
		return sccode;
	}
	public void setSccode(String sccode) {
		this.sccode = sccode;
	}
	public String getTownship() {
		return township;
	}
	public void setTownship(String township) {
		this.township = township;
	}
	public int getActivity() {
		return activity;
	}
	public void setActivity(int activity) {
		this.activity = activity;
	}
	public String getVilageName() {
		return vilageName;
	}
	public void setVilageName(String vilageName) {
		this.vilageName = vilageName;
	}
	
	public SchoolSearchData()
	{
		this.activity = 0 ;
		this.sccode = "";
		this.township = "";
		this.vilageName = "";
		this.offset = 0;
		this.limit = 0;
	}
	

}
