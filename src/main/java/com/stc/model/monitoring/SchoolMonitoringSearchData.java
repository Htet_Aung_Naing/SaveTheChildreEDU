package com.stc.model.monitoring;

import java.io.Serializable;
import java.util.Date;

public class SchoolMonitoringSearchData implements Serializable{
	

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 66044689797140726L;
	String sccode;
	String township;
	int activity;
	String vilageName;
	int offset;
	int limit;
	Date fromMonitoringDate;
	Date toMonitoringDate;
	Date fromEstabllishDate;
	Date toEstablishDate;
	
	public Date getFromEstabllishDate() {
		return fromEstabllishDate;
	}
	public void setFromEstabllishDate(Date fromEstabllishDate) {
		this.fromEstabllishDate = fromEstabllishDate;
	}
	public Date getToEstablishDate() {
		return toEstablishDate;
	}
	public void setToEstablishDate(Date toEstablishDate) {
		this.toEstablishDate = toEstablishDate;
	}
	public Date getFromMonitoringDate() {
		return fromMonitoringDate;
	}
	public void setFromMonitoringDate(Date fromMonitoringDate) {
		this.fromMonitoringDate = fromMonitoringDate;
	}
	public Date getToMonitoringDate() {
		return toMonitoringDate;
	}
	public void setToMonitoringDate(Date toMonitoringDate) {
		this.toMonitoringDate = toMonitoringDate;
	}
	public String getSccode() {
		return sccode;
	}
	public void setSccode(String sccode) {
		this.sccode = sccode;
	}
	public String getTownship() {
		return township;
	}
	public void setTownship(String township) {
		this.township = township;
	}
	public int getActivity() {
		return activity;
	}
	public void setActivity(int activity) {
		this.activity = activity;
	}
	public String getVilageName() {
		return vilageName;
	}
	public void setVilageName(String vilageName) {
		this.vilageName = vilageName;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	public  SchoolMonitoringSearchData()
	{
		this.activity = 0 ;
		this.limit = 0;
		this.offset = 0;
		this.sccode = "";
		this.township = "";
		this.vilageName = "";

	}

}
