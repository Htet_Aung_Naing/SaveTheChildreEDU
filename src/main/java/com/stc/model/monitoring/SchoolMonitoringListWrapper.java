package com.stc.model.monitoring;



import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Htet Aung Naing on 10/25/2016.
 */

public class SchoolMonitoringListWrapper implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -606367666436495172L;
	List<SchoolMonitoringData> schoolLsit;

    public List<SchoolMonitoringData> getSchoolLsit() {
        return schoolLsit;
    }

    public void setSchoolLsit(List<SchoolMonitoringData> schoolLsit) {
        this.schoolLsit = schoolLsit;
    }

    public SchoolMonitoringListWrapper()
    {
        this.schoolLsit = new ArrayList<SchoolMonitoringData>() ;
    }
}
