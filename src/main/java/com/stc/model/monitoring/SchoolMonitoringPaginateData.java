package com.stc.model.monitoring;

import java.io.Serializable;
import java.util.List;

public class SchoolMonitoringPaginateData implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4053683865775094977L;
	
	int count;
	List<SchoolMonitoringData> schoolList;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<SchoolMonitoringData> getSchoolList() {
		return schoolList;
	}
	public void setSchoolList(List<SchoolMonitoringData> schoolList) {
		this.schoolList = schoolList;
	}
	

}
